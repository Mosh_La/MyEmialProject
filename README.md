Welcome to my DEmailS - Dawson Emailing system and here is how to use it: <br/>
-------------------------------------------------------------------------------------------------------------------------------------------------------------------


<b>Assumptions</b>
* Make sure you are connected to the internet. 
* Make sure mysql is running in services

<b>Steps:</b> <br/>
<b>------------------------ Creating a database for the TESTS ------------------------ </b><br/>
<b>In MySQLWorkBench create a new database connection:</b>
* database (icon) -> new connection -> name it: <b>testdatabase </b> ->  press apply at the buttom 

<b>Add that connection to netbeans</b>

* Go to netBeans -> window -> services -> database <br/>
 right click on database -> add a new connection:

<b>name:</b> root <br/> 
<b>password: </b> dawson <br/>
<b>database Name: </b> testdatabase (the one we just created) <br/>

<b>Run the files: </b>
* run the <b>src/main/res/test_auth.sql</b> file to create new user and grant it privillage
* run the <b>src/main/res/test_seed.sql </b>file to create the database schema, tables..

<b>RUN MAVEN</b> 
<br/>

<b>------------------------ Creating a database for the PROGRAM ------------------------ <b/><br/>

<b>In MySQLWorkBench create a new database connection:</b>
* database (icon) -> new connection -> name it: <b>database1 </b> ->  press apply at the buttom 

<b>Add that connection to netbeans</b>

* Go to netBeans -> window -> services -> database </br>
 right click on database -> add a new connection:

<b>name:</b> root <br/> 
<b>password: </b> dawson <br/>
<b>database Name: </b> database1 (the one we just created) <br/>

<b>Run the files: </b>
* run the <b>src/main/res/auth.sql</b> file to create new user and grant it privillage
* run the <b>src/main/res/seed.sql </b>file  to create the database schema, tables..

<b>RUN MAVEN</b> 

<br/>

<b>NOTE:</b> if it is the first time you are running it:
a properties page will pop up, enter the information that is hinted :)
Else, if it's the second time, you are already "logged-in".
<b>To test it:</b> try to deleting the MailConfig.properties file which is in the app's <b>\MyEmialProject</b> folder. <br/>


<b>------------------------ When loading the application ------------------------ </b> <br/>

<b> you are redirected to the MAIN PAGE</b> that contains:<br/>
-----------------------------------1--------------------------------------------<br/>
* Folders listView -> click on any folder to view its assosiate emails<br/>
  There are 2 buttons assosiated with the manipulation of the folders <br/>
<b>-------> </b> ADD FOLDER BUTTON - will add a folder to the list  <br/>
 * Will validate that the folder doesn't exist already in the database <br/>
 * Will validate that the name is not "" (empty) <br/>
 
<b>-------> </b> DELETE FOLDER BUTTON - will delete the selected folder in the ListView! <br/>
* MUST SELECT A FOLDER IN ORDER FOR THE DELETE TO WORK. <br/>
* In other words, stand on the folder's name in that listView and then press on delete. <br/>

------------------------------------2-------------------------------------------<br/>

* TableView -> contains all the emails that are assosiated with a particular folder <br/>
  EX: to see all the emails in the "SENT" category click on the send folders 
* On top , there are 7 buttons to manipulate the emails presented in the table
</br>
-------------------------------------------------------------------------------Buttons------------------------------------------------------------------------------------<br/>

* COMPOSE BUTTON -> This button will redirect you to the send page where you can write an email <br>
         * In it, you will have an option to add a regular attachment or an embedded one <br> <br/>
         * <b>NOTE: </b> to add an embedded attachment, you may only drag and drop it from the FileChooser that pops up when clicking on the "embedded attachment" button <br/>
         * You also have the option to save the email to draft: <br> <br/>
         * You can later edit the draft by going back to the MainPage and cliking on the DRAFT folder.<br/>
         * The program will validate that there is at LEAST ONE valid receiver. Try entering nothing or blah blah blah and then click on send:) (it will not) <br/>
         * To add more than one receiver, please follow the following syntax: x, y, z (comma separated!) <br/>
         * When you add an regular attachment, a blue button with the name of the attachment will appear to represent the added attachment, if you click on it, it will ask you
          if you are willing to delete it.
         * And most importantly, you can SEND an email by clicking on the button. <br/>
         

-------------------------------------------------------------------------------------------------------------------------------------------------------------------<br/>

* DELETE BUTTON -> Select the email you want to delete from the tableView and click on the DELETE button. <br/>
<b>NOTE: </b> To see your changes, click on the folder you are currently on. <br/>
<b>Ex: </b> When deleting an email from folder X, make sure you click again on folder X after deleting the email, to see the changes! <br/>

-------------------------------------------------------------------------------------------------------------------------------------------------------------------<br/>

* SHOW BUTTON-> To view an email, you must select it from the TableView and click on SHOW button.
* -> If you are selecting an email from the draft folder: <b>SHOW -> Will be redirected to the SEND PAGE, where the user can edit the email.</b> 
        Else: <b>it will be redirected to the SHOW PAGE, a read-only page</b>
        In the read only page, to download the regular attachment, press on the blue button that has the name of your attachment, and chose the file location you want to store it at.
* You can also move the currenly showen email to another folder!<br/>

-------------------------------------------------------------------------------------------------------------------------------------------------------------------<br/>

* SETTINGS BUTTON -> Has the options to change the properties file and tells the user some info about the application.
* <b>NOTE: </b> If you change the properties and go back to the main page, the program will crush. </br>
* Therefore, it is better to change the properties, CLOSE THE APP and RUN MAVEN again.
* BUT: if you change the name, its okay:) - try it out, for testing!
* PS: make sure that the credentials are good.
* Another feature available on the setting page, is the About Button
* It will show some information about the program and will redirect to this readMe file & video file on how to use my program.
* Here is the link for the video: https://drive.google.com/open?id=19ecXXwGSMbv6gCv8HTof2I9JDufFFcA0

-------------------------------------------------------------------------------<br/>

* Reply, ReplyAll and Forward are some other features that my emailing system supports.

----------------------------BUGS----------------------<br/>
Unable to SEND draft -> gives authentication error.

