
DROP DATABASE IF EXISTS testdatabase;
CREATE DATABASE testdatabase;

DROP USER IF EXISTS laraTest@localhost;
CREATE USER laraTest@'localhost' IDENTIFIED BY 'lara';

GRANT ALL ON testdatabase.* TO laraTest@'localhost';

-- This creates a user with access from any IP number except localhost
-- Use only if your MyQL database is on a different host from localhost
-- DROP USER IF EXISTS root;
-- CREATE USER root IDENTIFIED WITH mysql_native_password BY 'pancake' REQUIRE NONE;
-- GRANT ALL ON GAMER_DB TO root;

FLUSH PRIVILEGES;