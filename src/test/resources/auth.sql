
DROP DATABASE IF EXISTS database1;
CREATE DATABASE database1;

DROP USER IF EXISTS lara@localhost;
CREATE USER lara@'localhost' IDENTIFIED BY 'lara';

GRANT ALL ON database1.* TO lara@'localhost';

-- This creates a user with access from any IP number except localhost
-- Use only if your MyQL database is on a different host from localhost
-- DROP USER IF EXISTS root;
-- CREATE USER root IDENTIFIED WITH mysql_native_password BY 'pancake' REQUIRE NONE;
-- GRANT ALL ON GAMER_DB TO root;

FLUSH PRIVILEGES;