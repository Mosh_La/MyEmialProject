USE DATABASE1;

-- tables
DROP TABLE IF EXISTS Attachment;
DROP TABLE IF EXISTS ReceiversList;
DROP TABLE IF EXISTS Folder_Email;
DROP TABLE IF EXISTS Folder;
DROP TABLE IF EXISTS Email;


CREATE TABLE Attachment (
    attachment_id int(30) NOT NULL AUTO_INCREMENT,
    content LONGBLOB NOT NULL,
    name varchar(30) NOT NULL,
    attachType bool NOT NULL,
    email_id int(30) NOT NULL,
    CONSTRAINT Attachment_pk PRIMARY KEY (attachment_id)
);

-- Table: Email
CREATE TABLE Email (
    email_id int(30) NOT NULL AUTO_INCREMENT,
    senderEmailAddress varchar(254) NOT NULL,
    subject varchar(78) NULL,
    htmlText varchar(8000)  NULL,
    PlainText varchar(8000) NULL,
    CONSTRAINT Email_pk PRIMARY KEY (email_id)
);

-- Table: Folder
CREATE TABLE Folder (
    folderName varchar(30) NOT NULL,
    CONSTRAINT Folder_pk PRIMARY KEY (folderName)
);

-- Table: Folder_Email
CREATE TABLE Folder_Email (
    email_id int(30) NOT NULL,
    folderName varchar(30) NOT NULL,
    CONSTRAINT Folder_Email_pk PRIMARY KEY (email_id,folderName)
);

-- Table: ReceiversList
CREATE TABLE ReceiversList (
    email_id int(30) NOT NULL,
    receiverType int(1) NOT NULL,
    receiverEmailAddress varchar(254) NOT NULL,
    CONSTRAINT ReceiversList_pk PRIMARY KEY (email_id, receiverType,receiverEmailAddress)
);

-- foreign keys
-- Reference: Attachment_Email (table: Attachment)
ALTER TABLE Attachment ADD CONSTRAINT Attachment_Email FOREIGN KEY Attachment_Email (email_id)
    REFERENCES Email (email_id) ON DELETE CASCADE;

-- Reference: ReceiversList_Email (table: ReceiversList)
ALTER TABLE ReceiversList ADD CONSTRAINT ReceiversList_Email FOREIGN KEY ReceiversList_Email (email_id)
    REFERENCES Email (email_id) ON DELETE CASCADE;

-- Reference: emailFolder_Email (table: Folder_Email)
ALTER TABLE Folder_Email ADD CONSTRAINT emailFolder_Email FOREIGN KEY emailFolder_Email (email_id)
    REFERENCES Email (email_id) ON DELETE CASCADE;

-- Reference: emailFolder_Folder (table: Folder_Email)
ALTER TABLE Folder_Email ADD CONSTRAINT emailFolder_Folder FOREIGN KEY emailFolder_Folder (folderName)
    REFERENCES Folder (folderName) ON DELETE CASCADE;

-- Data: add init data
INSERT INTO Folder VALUES("Sent");
INSERT INTO Folder VALUES("Received");
INSERT INTO Folder VALUES("Draft");
