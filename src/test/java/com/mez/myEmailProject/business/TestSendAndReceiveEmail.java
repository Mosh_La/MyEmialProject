/*
 * This class will test if an email is in the same state it was sent when received 
 */
package com.mez.myEmailProject.business;

import com.mez.myEmailProject.data.AttachmentBean;
import com.mez.myEmailProject.data.EmailBean;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author user
 * @version 1.0
 */
@Ignore
public class TestSendAndReceiveEmail {

    private String sender, senderPass, userName;
    List<String> receiver, cc, bcc;
    private File txtFileOne, txtFileTwo, jpg;
    private AttachmentBean attachmentOne, attachmentTwo, attachmentThree, attachmentFour, attachmentFive;
    private List<AttachmentBean> oneRegularAttachTxt, listEmbeddedAttach, oneEmbeddedAttach, listRegularAttach, oneEmbeddedAttachmentImage;

    @Before
    public void init() throws IOException {
        this.userName = "Lara";
        this.sender = "send.1631492@gmail.com";
        this.senderPass = "Lubov4714";
        this.receiver = new ArrayList<>();
        this.cc = new ArrayList<>();
        this.bcc = new ArrayList<>();

        receiver.add("receive.1631492@gmail.com");
        cc.add("laramo1999@gmail.com");
        bcc.add("laramo1999@gmail.com");
        this.txtFileOne = new File("test.txt");
        this.txtFileTwo = new File("test.txt");
        this.jpg = new File("testHello.jpg");

        this.attachmentOne = new AttachmentBean(txtFileOne, false);
        this.attachmentThree = new AttachmentBean(jpg, false);
        this.attachmentTwo = new AttachmentBean(txtFileTwo, true); //embedded
        this.attachmentFour = new AttachmentBean(jpg, true); //embedded image
        this.attachmentFive = new AttachmentBean(jpg, true); //embedded image
        this.oneRegularAttachTxt = new ArrayList();
        this.listEmbeddedAttach = new ArrayList();
        this.oneEmbeddedAttach = new ArrayList();
        this.listRegularAttach = new ArrayList();
        this.oneEmbeddedAttachmentImage = new ArrayList();
        this.oneRegularAttachTxt.add(attachmentOne);
        this.oneEmbeddedAttachmentImage.add(attachmentFour);

        this.oneEmbeddedAttach.add(attachmentTwo);

        this.listEmbeddedAttach.add(attachmentFive);
        this.listEmbeddedAttach.add(attachmentFour);

        this.listRegularAttach.add(attachmentOne);
        this.listRegularAttach.add(attachmentThree);

        //clean
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        receiveEmail.receiveEmail();
        ReceiveEmail otherEmail = new ReceiveEmail("other.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        otherEmail.receiveEmail();
    }

    @Test
    public void testFromToWithSubject() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(this.senderPass);
        emailTest.setToAddress(receiver);
        emailTest.setSubject("This is a from-to subject");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testFromToNoSubject() throws Exception {
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setToAddress(receiver);
        emailTest.setSubject("");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        emailTest.setSubject(null);
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testFromToEmbeddedHtml() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setToAddress(receiver);
        emailTest.setSubject("This is a subject:)");
        emailTest.setHtmlMessage("<html><head><body><p>hello</p></body></html>");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testFromPlainText() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setToAddress(receiver);
        emailTest.setSubject("This is a subject:)");
        emailTest.setPlainTextMessage("LaraLaraLara");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testCcAndHtmlPlusPlainText() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setSubject("This is a subject:)");
        emailTest.setPlainTextMessage("LaraLaraLara");
        emailTest.setHtmlMessage("<html><head></head><body><p>Java3!</p></body></html>");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testFromCcSubject() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setSubject("SUBJECT!");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testFromBCcSubject() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setBcc(bcc);
        emailTest.setSubject("This is a test for cc and bcc!");
        emailTest.setHtmlMessage("<html><head></head><body><p>Java3!</p></body></html>");
        emailTest.setPlainTextMessage("This is a test for the cc and bcc!");
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFromWithoutTo() throws Exception {

        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
    }
//testing attachments

    @Test
    public void testAttachmentAndManyCC() throws Exception {
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        cc.add("other.1631492@gmail.com");
        emailTest.setCc(cc);
        emailTest.setBcc(bcc);
        emailTest.setToAddress(receiver);
        emailTest.setSubject("This is a from-to subject");
        emailTest.setAttachment(this.oneRegularAttachTxt);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testManyAttachmentsAttached() throws Exception {
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setSubject("Many Attachments are going to be attached to this email");
        emailTest.setAttachment(this.listRegularAttach);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testManyEmbeddedAttachments() throws Exception {
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setToAddress(receiver);
        emailTest.setSubject("Many EMBEDDED Attachments are going to be attached to this email");
        emailTest.setEmbeddedAttachment(this.listEmbeddedAttach);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testEveythingInOneEmail() throws Exception {
        //also tests two same types of attachments together 
        List<String> receiveList = new ArrayList<>();
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setBcc(bcc);
        emailTest.setSubject("Testing all the compontents of an email together");
        emailTest.setHtmlMessage("<html><head></head><body><p>TEST ALL!</p></body></html>");
        emailTest.setSubject("Many Attachments are going to be attached to this email");
        emailTest.setAttachment(this.oneRegularAttachTxt);
        emailTest.setEmbeddedAttachment(this.oneEmbeddedAttachmentImage);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testEveythingInOneEmail2() throws Exception {
        //also tests two different types of attachments together 
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setBcc(bcc);
        emailTest.setSubject("Testing all the compontents of an email together");
        emailTest.setHtmlMessage("<html><head></head><body><p>TEST ALL!</p></body></html>");
        emailTest.setSubject("Many Attachments are going to be attached to this email");
        emailTest.setAttachment(this.oneRegularAttachTxt);
        emailTest.setEmbeddedAttachment(this.oneEmbeddedAttachmentImage);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }

    @Test
    public void testFoldersReceiveAndSend() throws Exception {
        //also tests two different types of attachments together 
        EmailBean emailTest = new EmailBean();
        emailTest.setUserName(userName);
        emailTest.setSenderAddress(sender);
        emailTest.setSenderPass(senderPass);
        emailTest.setCc(receiver);
        emailTest.setBcc(bcc);
        emailTest.setSubject("Testing all the compontents of an email together");
        emailTest.setHtmlMessage("<html><head></head><body><p>TEST ALL!</p></body></html>");
        emailTest.setSubject("Many Attachments are going to be attached to this email");
        emailTest.setAttachment(this.oneRegularAttachTxt);
        emailTest.setEmbeddedAttachment(this.oneEmbeddedAttachmentImage);
        SendEmail sendEmail = new SendEmail(emailTest, "smtp.gmail.com");
        sendEmail.sendEmail();
        Thread.sleep(8000);
        ReceiveEmail receiveEmail = new ReceiveEmail("receive.1631492@gmail.com", "Lubov81118", "imap.gmail.com");
        List<EmailBean> received = receiveEmail.receiveEmail();
        assertTrue(emailTest.equals(received.get(0)));
    }
}
