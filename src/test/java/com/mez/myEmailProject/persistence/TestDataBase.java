/*
 * This is a test class for phase2, where its testing storing and restoring EmailBeans from database
 */
package com.mez.myEmailProject.persistence;

import com.mez.myEmailProject.data.AttachmentBean;
import com.mez.myEmailProject.data.EmailBean;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author 1631492
 * @version 1.0
 */

public class TestDataBase {

    private String url = "jdbc:mysql://localhost:3306/testdatabase?";
    private String sender, senderPass, userName;
    List<String> receiver, cc, bcc;
    private File txtFileOne, txtFileTwo, jpg;
    private AttachmentBean attachmentOne, attachmentTwo, attachmentThree, attachmentFour, attachmentFive;
    private List<AttachmentBean> oneRegularAttachTxt, listEmbeddedAttach, oneEmbeddedAttach, listRegularAttach, oneEmbeddedAttachmentImage;

    public TestDataBase() {
    }

    @Before
    public void setUp() throws IOException {
        final String seedDataScript = loadAsString("test_seed.sql");
        try (Connection connection = DriverManager.getConnection(url, "laraTest", "lara")) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
        //creating the restoredEmail components 
        this.userName = "Lara";
        this.sender = "send.1631492@gmail.com";
        this.senderPass = "Lubov4714";
        this.receiver = new ArrayList<>();
        this.cc = new ArrayList<>();
        this.bcc = new ArrayList<>();

        receiver.add("receive.1631492@gmail.com");
        cc.add("laramo1999@gmail.com");
        bcc.add("laramo1999@gmail.com");
        this.txtFileOne = new File("test.txt");
        this.txtFileTwo = new File("test.txt");
        this.jpg = new File("testHello.jpg");

        this.attachmentOne = new AttachmentBean(txtFileOne, false);
        this.attachmentThree = new AttachmentBean(jpg, false);
        this.attachmentTwo = new AttachmentBean(txtFileTwo, true); //embedded
        this.attachmentFour = new AttachmentBean(jpg, true); //embedded image
        this.attachmentFive = new AttachmentBean(jpg, true); //embedded image
        this.oneRegularAttachTxt = new ArrayList();
        this.listEmbeddedAttach = new ArrayList();
        this.oneEmbeddedAttach = new ArrayList();
        this.listRegularAttach = new ArrayList();
        this.oneEmbeddedAttachmentImage = new ArrayList();
        this.oneRegularAttachTxt.add(attachmentOne);
        this.oneEmbeddedAttachmentImage.add(attachmentFour);

        this.oneEmbeddedAttach.add(attachmentTwo);

        this.listEmbeddedAttach.add(attachmentFive);
        this.listEmbeddedAttach.add(attachmentFour);

        this.listRegularAttach.add(attachmentOne);
        this.listRegularAttach.add(attachmentThree);
    }

    @After
    public void tearDown() {
        //clear the emails
    }

    @Test
    public void storeAndGetEmptyEmailWithSubject() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Comparing Different Emails - Subject",
                "This email has plainText",
                "",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int id = store.storeEmail(storedEmail, "Test"); // the id the restoredEmail has been stored at
        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(id);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void compareTwoDifferentEmailsIdTestTextHTMLAndRegular() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmailOne = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Comparing Different Emails - Subject",
                "",
                "<html><head></head><body><p>This email has html</p></body></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        EmailBean storedEmailTwo = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Comparing Different Emails - Subject",
                "This email has plainText",
                "",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");

        //store email
        int idOne = store.storeEmail(storedEmailOne, "Test");
        int idTwo = store.storeEmail(storedEmailTwo, "Test");

        //compare both email's id
        assertFalse(idOne == idTwo);
    }

    @Test
    public void compareTwoDifferentEmailsWithBothTextFields() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmailOne = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject - number 1",
                "I have text now",
                "<html><head><body><div>I am a div!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        EmailBean storedEmailTwo = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject - number 2",
                "I have text now",
                "<html><head><body><div>I am a div!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");

        //store email
        int idOne = store.storeEmail(storedEmailOne, "Test");
        int idTwo = store.storeEmail(storedEmailTwo, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");

        //find the restoredEmail with that Id
        EmailBean emailOne = restore.getEmailByEmailId(idOne);
        EmailBean emailTwo = restore.getEmailByEmailId(idTwo);
        //compare both email's together
        assertFalse(emailOne.equals(emailTwo));
    }

    @Test
    public void getAnEmailThatAintStored() throws SQLException {
        //create restoredEmail to send
        EmailBean storedEmailOne = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing an email that has not been stored",
                "",
                "",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean noSuchEmailInDB = restore.getEmailByEmailId(storedEmailOne.getId()); //will be -1
        //email will be creted with null values 
        assertNull(noSuchEmailInDB = null);

    }

    @Test
    public void setAndGetEmailWithSubjectAndPlainText() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject and plainText",
                "I have text now",
                "",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");

        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");

        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetEmailWithTwoTypesOfText() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject!",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetAllTextTypeAndCc() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject!",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetAllTextTypeAndMANYCc() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject!",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetManyCCandOneBcc() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing subject!",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                this.bcc,
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetManyCCandManyBcc() throws SQLException, IOException {
        //create restoredEmail to send
        cc.add("send.1631492@gmail.com");
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "This email has many cc's and many bcc's",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                this.bcc,
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetManyCCandManyBccNoTo() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "This email has many cc's and many bcc's",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                this.bcc,
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email.
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void setAndGetMultipleTypesOfReceiversAll() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "This email has many cc's and many bcc's",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                this.bcc,
                Collections.<AttachmentBean>emptyList(),
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test(expected = IllegalArgumentException.class)
    public void emailWithNoRecepiants() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                Collections.<String>emptyList(),
                "Testing one regular attach",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                oneRegularAttachTxt,
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void emailWithRegAttach() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing one regular attach",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                oneRegularAttachTxt,
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void emailWithEmbeddedAttach() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing one embedded attach",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                this.oneEmbeddedAttach);

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void emailWithManyEmbeddedAttach() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing many embedded attach",
                "I have text now",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                Collections.<AttachmentBean>emptyList(),
                this.listEmbeddedAttach);

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void emailWithManyRegAttach() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing many regular attachments",
                "I have text now!",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                Collections.<String>emptyList(),
                Collections.<String>emptyList(),
                this.listRegularAttach,
                Collections.<AttachmentBean>emptyList());

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void allManyEmailComponents() throws SQLException, IOException {
        //create restoredEmail to send
        List<String> test = new ArrayList<>();
        test.add("liana4714@gmail.com");
        test.add("receiver.1631492@gmail.com");
        cc.add("send.1631492@gmail.com");
        bcc.add("other.1631492@gmail.com");
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                test,
                "Testing all email components as manyyy!",
                "I have text now!",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                this.bcc,
                this.listRegularAttach,
                this.listEmbeddedAttach);

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    @Test
    public void allSingalEmailComponents() throws SQLException, IOException {
        //create restoredEmail to send
        EmailBean storedEmail = new EmailBean(
                -1,
                this.userName,
                this.sender,
                this.senderPass,
                this.receiver,
                "Testing allllll email components as units only ",
                "I have text now!",
                "<html><head><body><div>I am a div in html!</div></body></head></html>",
                this.cc,
                this.bcc,
                this.oneRegularAttachTxt,
                this.oneEmbeddedAttach);

        StoreEmail store = new StoreEmail(this.url, "laraTest", "lara");
        //store email
        int idOne = store.storeEmail(storedEmail, "Test");

        //retreive email
        RestoreEmail restore = new RestoreEmail(this.url, "laraTest", "lara");
        //find the restoredEmail with that Id
        EmailBean restoredEmail = restore.getEmailByEmailId(idOne);
        //compare both email's together
        assertTrue(restoredEmail.equals(storedEmail));
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
