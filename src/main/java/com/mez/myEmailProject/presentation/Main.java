package com.mez.myEmailProject.presentation;

import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.controller.MainFXMLController;
import com.mez.myEmailProject.controller.PropFXMLController;
import com.mez.myEmailProject.manager.PropertiesManager;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main class author Lara
 */
public class Main extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(Main.class);

    private MainFXMLController main;
    private Stage stage;
    private Util util = new Util();

    public Main() {
        super();
    }

    /**
     * Start the JavaFX application
     *
     * @param primaryStage
     * @throws java.lang.Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        this.stage.getIcons().add(
                new Image(Main.class
                        .getResourceAsStream("/images/icon.png")));
        Scene scene2 = createRestOfTheProgram();
        Scene scene1 = createPropertiesForm(scene2);
        if (!checkForProperties()) {
            this.stage.setScene(scene1);
            LOG.info("Showing properties scene since no properties file detected");
        } else {
            main.receiveEmails();
            main.getFolders();
            main.showFirstElementInFolderList();
            this.stage.setScene(scene2);
            main.setStage(stage);
            LOG.info("Showing main page scene since properties file was detected");

        }
        this.stage.setTitle("Welcome - Main Page");
        this.stage.show();

    }

    /**
     * Create a stage with the scene showing the primary program window (aka
     * main page)
     *
     * @return Scene
     * @throws Exception (general)
     */
    private Scene createRestOfTheProgram() throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/MainPage.fxml"));
        Parent root = loader.load();
        main = loader.getController();
        main.setStage(this.stage);
        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * Assuming that properties are not found then here is the properties window
     *
     * @param scene2
     * @throws Exception
     */
    private Scene createPropertiesForm(Scene scene2) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/Prop.fxml"));
        Parent root = loader.load();
        PropFXMLController controller = loader.getController();
        controller.setSceneStageController(scene2, stage, main);
        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * Check if a Properties file exists and can be loaded into a bean. This
     * does not verify that the contents of the bean fields are valid.
     *
     * @return found if properties file exits
     */
    private boolean checkForProperties() {
        boolean found = false;
        FXConfig conf = new FXConfig();
        PropertiesManager pm = new PropertiesManager();

        try {
            if (pm.loadTextProperties(conf, "", "MailConfig")) {
                found = true;
            }
        } catch (IOException ex) {
            LOG.error("checkForProperties error", ex);
        }
        return found;
    }

    /**
     * The main() method
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
