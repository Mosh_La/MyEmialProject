package com.mez.myEmailProject.fxData;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author 1631492
 */
public class FXConfig {

    private StringProperty userName, userEmailAddress, userPass, imapServer,
            smtpServer, dbName, dbPort, dbUserName, dbUserPass, url, imapPort, smtpPort;

    /**
     * Default Constructor
     */
    public FXConfig() {
        this("", "", "", "", "", "", "", "", "", "", "", "");
    }

    /**
     * Constructor to set SimpleStringProperty values
     *
     * @param userName
     * @param userEmailAddress
     * @param userPass
     * @param imapServer
     * @param smtpServer
     * @param imapPort
     * @param smtpPort
     * @param dbPort
     * @param dbName
     * @param dbPass
     * @param url
     * @param userDbName
     * @param userDbPass
     */
    public FXConfig(String userName, String userEmailAddress, String userPass, String imapServer, String smtpServer, String imapPort, String smtpPort,
            String dbPort, String dbName, String url, String userDbName, String userDbPass) {
        super();
        this.userName = new SimpleStringProperty(userName);
        this.userEmailAddress = new SimpleStringProperty(userEmailAddress);
        this.userPass = new SimpleStringProperty(userPass);

        this.imapServer = new SimpleStringProperty(imapServer);
        this.smtpServer = new SimpleStringProperty(smtpServer);
        this.smtpPort = new SimpleStringProperty(smtpPort);
        this.imapPort = new SimpleStringProperty(imapPort);

        this.dbPort = new SimpleStringProperty(dbPort);
        this.dbName = new SimpleStringProperty(dbName);
        this.url = new SimpleStringProperty(url);

        this.dbUserName = new SimpleStringProperty(userDbName);
        this.dbUserPass = new SimpleStringProperty(userDbPass);
    }// end con

    public StringProperty getUserNameProperty() {
        return userName;
    }

    public StringProperty getUserEmailAddressProperty() {
        return userEmailAddress;
    }

    public StringProperty getUserPassProperty() {
        return userPass;
    }

    public StringProperty getImapServerProperty() {
        return imapServer;
    }

    public StringProperty getSmtpServerProperty() {
        return smtpServer;
    }

    public StringProperty getDbNameProperty() {
        return dbName;
    }

    public StringProperty getDbPortProperty() {
        return dbPort;
    }

    public StringProperty getDbUserNameProperty() {
        return dbUserName;
    }

    public StringProperty getDbUserPassProperty() {
        return dbUserPass;
    }

    public StringProperty getUrlProperty() {
        return url;
    }

    public StringProperty getImapPortProperty() {
        return imapPort;
    }

    public StringProperty getSmtpPortProperty() {
        return smtpPort;
    }

    public String getUserNameValue() {
        return userName.get();
    }

    public String getUserEmailAddressValue() {
        return userEmailAddress.get();
    }

    public String getUserPassValue() {
        return userPass.get();
    }

    public String getImapServerValue() {
        return imapServer.get();
    }

    public String getSmtpServerValue() {
        return smtpServer.get();
    }

    public String getDbNameValue() {
        return dbName.get();
    }

    public String getDbPortValue() {
        return dbPort.get();
    }

    public String getDbUserNameValue() {
        return dbUserName.get();
    }

    public String getDbUserPassValue() {
        return dbUserPass.get();
    }

    public String getUrlValue() {
        return url.get();
    }

    public String getImapPortValue() {
        return imapPort.get();
    }

    public String getSmtpPortValue() {
        return smtpPort.get();
    }

    public void setUserNameProperty(String userName) {
        this.userName.set(userName);
    }

    public void setUserEmailAddressProperty(String userEmailAddress) {
        this.userEmailAddress.set(userEmailAddress);
    }

    public void setUserPassProperty(String userPass) {
        this.userPass.set(userPass);
    }

    public void setImapServerProperty(String imapServer) {
        this.imapServer.set(imapServer);
    }

    public void setSmtpServerProperty(String smtpServer) {
        this.smtpServer.set(smtpServer);
    }

    public void setDbNameProperty(String dbName) {
        this.dbName.set(dbName);
    }

    public void setDbPortProperty(String dbPort) {
        this.dbPort.set(dbPort);
    }

    public void setDbUserNameProperty(String dbUserName) {
        this.dbUserName.set(dbUserName);
    }

    public void setDbUserPassProperty(String dbUserPass) {
        this.dbUserPass.set(dbUserPass);
    }

    public void setUrlProperty(String url) {
        this.url.set(url);
    }

    public void setImapPortProperty(String imapPort) {
        this.imapPort.set(imapPort);
    }

    public void setSmtpPortProperty(String smtpPort) {
        this.smtpPort.set(smtpPort);
    }
}
