package com.mez.myEmailProject.fxData;

import java.util.List;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author 1631492
 */
public class FXEmailBean {

    private IntegerProperty id;
    private StringProperty userName, senderAddress, senderPass, subject,
            plainTextMessage, htmlMessage, toAddress, cc, bcc;
    private ListProperty<FXAttachBean> attachment, embeddedAttachment;

    /**
     * Init fxEmailBean
     */
    public FXEmailBean() {
        userName = new SimpleStringProperty("");
        senderAddress = new SimpleStringProperty("");
        senderPass = new SimpleStringProperty("");
        id = new SimpleIntegerProperty(-1); //-1 because not stored in the db yet
        toAddress = new SimpleStringProperty("");
        cc = new SimpleStringProperty("");
        bcc = new SimpleStringProperty("");
        subject = new SimpleStringProperty("");
        plainTextMessage = new SimpleStringProperty("");
        htmlMessage = new SimpleStringProperty("");
        attachment = new SimpleListProperty<>(FXCollections.<FXAttachBean>observableArrayList());
        embeddedAttachment = new SimpleListProperty<>(FXCollections.<FXAttachBean>observableArrayList());
    }

    /**
     * Overloaded constructor that takes care of all the fields
     *
     * @param id
     * @param name
     * @param password
     * @param email
     * @param receiver
     * @param cc
     * @param bcc
     * @param sub
     * @param plainTextMsg
     * @param htmlMsg
     * @param attach
     * @param embeddedAttach
     */
    public FXEmailBean(int id, String name, String password, String email, String receiver, String cc, String bcc,
            String sub, String plainTextMsg, String htmlMsg, List<FXAttachBean> attach,
            List<FXAttachBean> embeddedAttach) {
        this();
        this.id = new SimpleIntegerProperty(id);
        this.userName = new SimpleStringProperty(name);
        this.senderPass = new SimpleStringProperty(password);
        this.senderAddress = new SimpleStringProperty(email);
        this.toAddress = new SimpleStringProperty(receiver);
        this.cc = new SimpleStringProperty(cc);
        this.bcc = new SimpleStringProperty(bcc);
        this.subject = new SimpleStringProperty(sub);
        this.plainTextMessage = new SimpleStringProperty(plainTextMsg);
        this.htmlMessage = new SimpleStringProperty(htmlMsg);
        this.attachment = new SimpleListProperty<>(FXCollections.<FXAttachBean>observableArrayList(attach));
        this.embeddedAttachment = new SimpleListProperty<>(FXCollections.<FXAttachBean>observableArrayList(embeddedAttach));
    }

    public IntegerProperty getIdProperty() {
        return id;
    }

    public StringProperty getUserNameProperty() {
        return userName;
    }

    public StringProperty getSenderAddressProperty() {
        return senderAddress;
    }

    public StringProperty getSenderPassProperty() {
        return senderPass;
    }

    public StringProperty getSubjectProperty() {
        return subject;
    }

    public StringProperty getPlainTextMessageProperty() {
        return plainTextMessage;
    }

    public StringProperty getHtmlMessageProperty() {
        return htmlMessage;
    }

    public StringProperty getToAddressProperty() {
        return toAddress;
    }

    public StringProperty getCcProperty() {
        return cc;
    }

    public StringProperty getBccProperty() {
        return bcc;
    }

    public ListProperty<FXAttachBean> getAttachmentProperty() {
        return attachment;
    }

    public ListProperty<FXAttachBean> getEmbeddedAttachmentProperty() {
        return embeddedAttachment;
    }

    public int getIdValue() {
        return id.get();
    }

    public String getUserNameValue() {
        return userName.get();
    }

    public String getSenderAddressValue() {
        return senderAddress.get();
    }

    public String getSenderPassValue() {
        return senderPass.get();
    }

    public String getSubjectValue() {
        return subject.get();
    }

    public String getPlainTextMessageValue() {
        return plainTextMessage.get();
    }

    public String getHtmlMessageValue() {
        return htmlMessage.get();
    }

    public String getToAddressValue() {
        return toAddress.get();
    }

    public String getCcValue() {
        return cc.get();
    }

    public String getBccValue() {
        return bcc.get();
    }

    public List<FXAttachBean> getAttachmentValue() {
        return attachment.get();
    }

    public List<FXAttachBean> getEmbeddedAttachmentValue() {
        return embeddedAttachment.get();
    }

    public void setIdProperty(int id) {
        this.id.set(id);
    }

    public void setUserNameProperty(String userName) {
        this.userName.set(userName);
    }

    public void setSenderAddressProperty(String senderAddress) {
        this.senderAddress.set(senderAddress);
    }

    public void setSenderPassProperty(String senderPass) {
        this.senderPass.set(senderPass);
    }

    public void setSubjectProperty(String subject) {
        this.subject.set(subject);
    }

    public void setPlainTextMessageProperty(String plainTextMessage) {
        this.plainTextMessage.set(plainTextMessage);
    }

    public void setHtmlMessageProperty(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }

    public void setToAddressProperty(String to) {
        this.toAddress.set(to);
    }

    public void setCcProperty(String cc) {
        this.cc.set(cc);

    }

    public void setBccProperty(String bcc) {
        this.bcc.set(bcc);

    }

    /**
     * Private helper method to check if stringProperty is empty
     */
    private boolean isEmptyProperty(SimpleStringProperty string) {
        return new SimpleStringProperty("").equals((string.get()));
    }

    /**
     * Sets attachments in FXEmailBean
     *
     * @param attachment
     */
    public void setAttachmentProperty(List<FXAttachBean> attachment) {
        ObservableList<FXAttachBean> list = FXCollections.observableArrayList(attachment);
        this.attachment.setAll(list);
    }

    /**
     * Sets embedded attachments in FXEmailBean
     *
     * @param embeddedAttachment
     */
    public void setEmbeddedAttachmentProperty(List<FXAttachBean> embeddedAttachment) {
        ObservableList<FXAttachBean> list = FXCollections.observableArrayList(embeddedAttachment);
        this.embeddedAttachment.setAll(embeddedAttachment);
    }

    /**
     * Overrding hash code because the equals method was overriden
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.senderAddress);
        hash = 67 * hash + Objects.hashCode(this.subject);
        hash = 67 * hash + Objects.hashCode(this.plainTextMessage);
        hash = 67 * hash + Objects.hashCode(this.htmlMessage);
        hash = 67 * hash + Objects.hashCode(this.toAddress);
        hash = 67 * hash + Objects.hashCode(this.cc);
        hash = 67 * hash + Objects.hashCode(this.attachment);
        hash = 67 * hash + Objects.hashCode(this.embeddedAttachment);
        return hash;
    }

    /**
     * Overriding the equals method -> email components equality
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FXEmailBean other = (FXEmailBean) obj;
        if (!Objects.equals(this.senderAddress, other.senderAddress)) {
            return false;
        }
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        if (!Objects.equals(this.plainTextMessage, other.plainTextMessage)) {
            return false;
        }
        if (!Objects.equals(this.htmlMessage, other.htmlMessage)) {
            return false;
        }
        if (!Objects.equals(this.toAddress, other.toAddress)) {
            return false;
        }
        if (!Objects.equals(this.cc, other.cc)) {
            return false;
        }
        if (!Objects.equals(this.attachment, other.attachment)) {
            return false;
        }
        return Objects.equals(this.embeddedAttachment, other.embeddedAttachment);
    }
}
