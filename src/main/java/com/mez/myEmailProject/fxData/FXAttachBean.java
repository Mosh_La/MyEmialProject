package com.mez.myEmailProject.fxData;

import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.data.AttachmentBean;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author 1631492
 * @version 1.0
 */
public class FXAttachBean {

    private StringProperty attachmentName;
    private ObjectProperty<byte[]> attachment;
    private StringProperty filePath;
    private BooleanProperty isEmbedded;
    private final Util util = new Util();
    private File file;

    /**
     * Constructor that accepts a file and a type
     *
     * @param file
     * @param isEmbedded
     */
    public FXAttachBean(File file, boolean isEmbedded) {
        try {
            this.file = file;
            this.filePath = new SimpleStringProperty(file.getPath());
            this.attachment = new SimpleObjectProperty(util.convertFileToByteArray(file));
            this.attachmentName = new SimpleStringProperty(file.getName());
            this.isEmbedded = new SimpleBooleanProperty(isEmbedded);
        } catch (IOException ie) {
            ie.getMessage();
        }
    }
    /**
     * overloaded constructor that takes all the fields of the fxAttachmentBean
     * @param bean 
     */
        public FXAttachBean(AttachmentBean bean) {
            this.file = bean.getFile();
            this.filePath = new SimpleStringProperty(bean.getFilePath());
            this.attachment = new SimpleObjectProperty(bean.getAttachment());
            this.attachmentName = new SimpleStringProperty(bean.getAttachmentName());
            this.isEmbedded = new SimpleBooleanProperty(bean.getIsEmbedded());
    }

    /**
     * Overloaded constructor that accepts file name, content, file path and
     * type
     *
     * @param array
     * @param isEmbedded
     * @param filePath
     * @param name
     */
    public FXAttachBean(SimpleStringProperty name, SimpleObjectProperty<byte[]> array,
            SimpleStringProperty filePath, SimpleBooleanProperty isEmbedded) {
        this.attachmentName = name;
        this.attachment = array;
        this.filePath = filePath;
        this.isEmbedded = isEmbedded;
    }

    public StringProperty getAttachmentNameProperty() {
        return attachmentName;
    }

    public ObjectProperty<byte[]> getAttachmentProperty() {
        return attachment;
    }

    public StringProperty getFilePathProperty() {
        return filePath;
    }

    public BooleanProperty getIsEmbeddedProperty() {
        return isEmbedded;
    }

    public String getAttachmentNameValue() {
        return attachmentName.get();
    }

    public byte[] getAttachmentByteValue() {
        return attachment.get();
    }

    public String getFilePathValue() {
        return filePath.get();
    }

    public boolean getIsEmbeddedBooleanValue() {
        return isEmbedded.get();
    }

    public void setAttachmentNameProperty(String attachmentName) {
        this.attachmentName.set(attachmentName);
    }

    public void setAttachmentProperty(byte[] attachment) {
        this.attachment.set(attachment);
    }

    public void setFilePathProperty(String filePath) {
        this.filePath.set(filePath);
    }

    public void setIsEmbeddedProperty(boolean isEmbedded) {
        this.isEmbedded.set(isEmbedded);
    }

    public File getFile() {
        return this.file;
    }

    /**
     * Overriding the equals method -> content and type equality
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FXAttachBean other = (FXAttachBean) obj;
        if (!Objects.equals(this.attachment, other.attachment)) {
            return false;
        }
        return Objects.equals(this.isEmbedded, other.isEmbedded);
    }

    /**
     * Overrding hash code because the equals method was overriden
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.attachment);
        hash = 79 * hash + Objects.hashCode(this.isEmbedded);
        return hash;
    }
}
