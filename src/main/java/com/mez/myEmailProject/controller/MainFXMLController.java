/**
 * This class represents the main page (navigator) of my email App
 */
package com.mez.myEmailProject.controller;

import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.business.ReceiveEmail;
import com.mez.myEmailProject.data.EmailBean;
import com.mez.myEmailProject.fxData.FXEmailBean;
import com.mez.myEmailProject.persistence.DeleteDAO;
import com.mez.myEmailProject.persistence.RestoreEmail;
import com.mez.myEmailProject.persistence.StoreEmail;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainFXMLController implements ControllerInterface {

    private Stage stage;
    private SendFXMLController sendController;
    private SettingFXMLController settingController;
    private ShowFXMLController showEmailController;
    private FXMLLoader loader;
    private final Util util;
    private List<String> folders;
    public final Stage newStage;
    private final static Logger LOG = LoggerFactory.getLogger(MainFXMLController.class);

    /**
     * Initialize constructor
     */
    public MainFXMLController() {
        util = new Util();
        newStage = new Stage();
        loader = new FXMLLoader();
        folders = new ArrayList<>();
    }

    @FXML
    private ListView<String> listOfFolders;

    @FXML
    private TableColumn<FXEmailBean, String> from;

    @FXML
    private TableColumn<FXEmailBean, String> subject;

    @FXML
    private TableColumn<FXEmailBean, String> content;
    @FXML
    private TableView<FXEmailBean> table;

    /**
     * Will delete a folder as long as it exist and it is not part of the
     * default folders
     *
     * @param event
     */
    @FXML
    void deleteFolder(ActionEvent event) throws SQLException, IOException {
        if (listOfFolders.getSelectionModel().getSelectedItems().get(0) != null) {
            String folder = listOfFolders.getSelectionModel().getSelectedItems().get(0);
            if ("Sent".equals(folder)
                    || "Received".equals(folder)
                    || "Draft".equals(folder)) {
                util.alertError("Can't delete default folders:(", "Error");
            } else {
                FXConfig fxCon = util.getFXConfig();
                if (!fxCon.getUserNameValue().equals("")) {
                    DeleteDAO update = new DeleteDAO(fxCon.getUrlValue(),
                            fxCon.getDbUserNameValue(),
                            fxCon.getDbUserPassValue());
                    update.deleteFolder(folder);
                    listOfFolders.getItems().remove(folder);
                    LOG.info("deleteFolder button pressed -> folder: " + folder + " - is now deleted");
                }
            }
        }
    }

    /**
     * Will re-send an email message delivered to one email address to a
     * possibly different email address(es)-> "Copy of email"
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void forward(ActionEvent event) throws IOException {
        LOG.info("forward button pressed");
        if (table.getSelectionModel().getSelectedItem() != null) {
            loader = new FXMLLoader(getClass().getResource("/fxml/Send.fxml"));
            Parent root = loader.load();
            sendController = loader.getController();
            sendController.setStage(stage);
            this.sendController.forward(table.getSelectionModel().getSelectedItem());
            stage.setScene(new Scene(root));
            stage.setTitle("Forward Email");
            stage.setResizable(false);
            stage.show();
        } else {
            util.alertError("Nothing was selected..", "Error");
        }
    }

    /**
     * Will create a new send-page with the senders address to reply
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void reply(ActionEvent event) throws IOException {
        if (table.getSelectionModel().getSelectedItem() != null) {
            loader = new FXMLLoader(getClass().getResource("/fxml/Send.fxml"));
            Parent root = loader.load();
            sendController = loader.getController();
            sendController.setStage(stage);
            this.sendController.reply(table.getSelectionModel().getSelectedItem());
            stage.setScene(new Scene(root));
            stage.setTitle("Forward Email");
            stage.setResizable(false);
            stage.show();
            LOG.info("reply button pressed");
        } else {
            util.alertError("Nothing was selected..", "Error");
        }
    }

    /**
     * Will create a new send-page with the senders address and CCs to reply
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void replyAll(ActionEvent event) throws IOException {
        if (table.getSelectionModel().getSelectedItem() != null) {
            loader = new FXMLLoader(getClass().getResource("/fxml/Send.fxml"));
            Parent root = loader.load();
            sendController = loader.getController();
            sendController.setStage(stage);
            this.sendController.replyAll(table.getSelectionModel().getSelectedItem());
            stage.setScene(new Scene(root));
            stage.setTitle("Forward Email");
            stage.setResizable(false);
            stage.show();
            LOG.info("replayAll was pressed");

        } else {
            util.alertError("Nothing was selected..", "Error");
        }
    }

    /**
     * If the email is part of the draft folder -> show will redirect the user
     * the the send page where he can edit the email. Otherwise, -> show will
     * redirect to a read-only page, show page.
     *
     * @param event
     */
    @FXML
    void showEmail(ActionEvent event) throws IOException, SQLException {
        if (table.getSelectionModel().getSelectedItem() != null) {

            if (listOfFolders.getSelectionModel().getSelectedItem().equals("Draft")) {
                loader = new FXMLLoader(getClass().getResource("/fxml/Send.fxml"));
                Parent root = loader.load();
                sendController = loader.getController();
                sendController.setStage(stage);
                //show will be the send page, to be able to edit the draft
                this.sendController.editDraft(table.getSelectionModel().getSelectedItem());
                stage.setScene(new Scene(root));
                stage.setTitle("Send Email");
                stage.setResizable(false);
                stage.show();
            } else {
                loader = new FXMLLoader(getClass().getResource("/fxml/ShowEmail.fxml"));
                Parent root = loader.load();
                showEmailController = loader.getController();
                showEmailController.setStage(stage);
                //show will be read only 
                getFolders();
                showEmailController.showEmail(table.getSelectionModel().getSelectedItem(), this.folders);
                stage.setScene(new Scene(root));
                stage.setTitle("Show Email");
                stage.setResizable(false);
                stage.show();
                LOG.info("showEmail was cliked");
            }
        } else {
            util.alertError("Nothing was selected...", "Error");
        }
    }

    /**
     * Overriding the method from the interface to keep track of the current
     * stage
     *
     * @param stage
     */
    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Redirects to the creation of the email page (by creating a new scene to
     * an existing stage)
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void compose(ActionEvent event) throws IOException {
        util.createSceneToGivenStage(this.stage, sendController, "Compose A New Email", "/fxml/Send.fxml").show();
        LOG.info("compose was pressed");
    }

    /**
     * Deletes chosen email from table
     *
     * @param event
     */
    @FXML
    void delete(ActionEvent event) throws IOException, SQLException {
        FXConfig fxCon = util.getFXConfig();
        if (!fxCon.getUserNameValue().equals("")) {
            DeleteDAO delete = new DeleteDAO(fxCon.getUrlValue(),
                    fxCon.getDbUserNameValue(),
                    fxCon.getDbUserPassValue());
            if (table.getSelectionModel().getSelectedItem() != null) {
                FXEmailBean email = table.getSelectionModel().getSelectedItem();
                int id = email.getIdValue();
                delete.deleteAttachment(id);
                delete.deleteAllReceivers(id);
                delete.deleteEmail(id);
                util.alertInfo("delete was pressed - deleting selcted email", "Email Deleted");

            } else {
                util.alertError("Nothing was selected.. Delete didn't happend", "Error");
            }
            LOG.info("delete was pressed");

        }
    }

    /**
     * Redirects to the creation of the setting page (by creating a new scene to
     * existing stage)
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void openSettings(ActionEvent event) throws IOException {
        loader.setLocation(this.getClass().getResource("/fxml/Settings.fxml"));
        Parent root = loader.load();
        settingController = loader.getController();
        settingController.setStage(stage);
        stage.setScene(new Scene(root));
        stage.setTitle("Setting");
        stage.setResizable(false);
        stage.show();
        LOG.info("openSetting was pressed");
    }

    /**
     * Dialog box to create a new folder Will add folder name to the list and to
     * database if valid valid: Not "", and doesn't already exist
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void addFolder(ActionEvent event) throws IOException, SQLException {
        try {
            String folder = util.alertWithButtonsWithInput("Add Folder", "Enter Folder Name:", "Add").toLowerCase().trim();
            String validFolderName = folder.substring(0, 1).toUpperCase() + folder.substring(1);
            if (folder.isEmpty() || folder.length() == 0) {
                util.alertError("Invalid folder name", "Error");
                LOG.info("folder has an invalid name");
            } else {

                FXConfig fxCon = util.getFXConfig();
                if (!fxCon.getUserNameValue().equals("")) {

                    try {
                        StoreEmail store = new StoreEmail(fxCon.getUrlValue(),
                                fxCon.getDbUserNameValue(),
                                fxCon.getDbUserPassValue());
                        store.addFolder(validFolderName);
                        listOfFolders.getItems().add(validFolderName);
                        util.alertInfo("Adding a new Folder", "Add");
                        LOG.info("folder: " + validFolderName + " is added");

                    } catch (SQLException ex) {
                        util.alertError("Folder " + validFolderName + " already exists", "Error");
                        LOG.info("folder: " + validFolderName + " already exists");
                    }

                }
            }
        } catch (StringIndexOutOfBoundsException ex) {
        }
    }

    /**
     * When the MainPage is loaded, the method will extract all folders from db
     *
     * @throws SQLException
     * @throws IOException
     */
    public void getFolders() throws SQLException, IOException {

        FXConfig fxCon = util.getFXConfig();
        if (!fxCon.getUserNameValue().equals("")) {
            RestoreEmail restore = new RestoreEmail(fxCon.getUrlValue(),
                    fxCon.getDbUserNameValue(),
                    fxCon.getDbUserPassValue());
            this.folders = restore.getAllFolders(listOfFolders);
        }
    }

    /**
     * Will select the first item of the listView in the main page The default
     * selected folder is sent, will show all selected emails when loading the
     * main page (by default)
     */
    public void showFirstElementInFolderList() {
        listOfFolders.getSelectionModel().select(0);
    }

    /**
     * Will return the name of the folder that is selected, will be used to
     * query the database
     *
     * @param event
     * @return
     */
    @FXML
    void matchEmailsAccordingToFolder(MouseEvent e) throws SQLException, IOException {
        String folder = listOfFolders.getSelectionModel().getSelectedItem();
        FXConfig fxCon = util.getFXConfig();
        if (!fxCon.getUserNameValue().equals("")) {

            RestoreEmail restore = new RestoreEmail(fxCon.getUrlValue(),
                    fxCon.getDbUserNameValue(),
                    fxCon.getDbUserPassValue());
            //get all emails id in the given folder
            List<Integer> id = restore.findIdsForGivenFolder(folder);
            List<EmailBean> listOfEmails = new ArrayList();
            for (int i : id) {
                listOfEmails.add(restore.getEmailByEmailId(i));
            }
            LOG.info("There were" + listOfEmails.size() + " emails found in " + folder);

            List<FXEmailBean> listFXEmailBeans = new ArrayList<>();
            for (EmailBean bean : listOfEmails) {
                //create an fxbean
                LOG.info("this is the toString************see if probelm is with bean or not*********************" + bean.toString());
                listFXEmailBeans.add(bean.createFxBean());

            }
            table.setItems(FXCollections.observableArrayList(listFXEmailBeans));
        }
    }

    /**
     * This method will set the cellctory(bean -> bean.getValue().getHtmlMessageProperty());
    }

    public void receiveEmails() throws SQLException, IOException {
        try {
            FXConfig fxCon = util.getFXConfig();
            if (!fxCon.getUserNameValue().equals("")) {

                Receive values to the specific (from, subject,
     * content) values of the current fxEmail object
     */
    @FXML
    void initialize() throws SQLException, IOException {
        from.setCellValueFactory(bean -> bean.getValue().getSenderAddressProperty());
        subject.setCellValueFactory(bean -> bean.getValue().getSubjectProperty());
        content.setCellValueFactory(bean -> bean.getValue().getHtmlMessageProperty());
    }

    public void receiveEmails() throws SQLException, IOException {
        try {
            FXConfig fxCon = util.getFXConfig();
            if (!fxCon.getUserNameValue().equals("")) {

                ReceiveEmail receive = new ReceiveEmail(fxCon.getUserEmailAddressValue(),
                        fxCon.getUserPassValue(), fxCon.getImapServerValue());
                //receive the emails
                List<EmailBean> listReceived = receive.receiveEmail(); //debug
                LOG.info("There were " + listReceived.size() + " received emails");
                //save them in the db
                StoreEmail store = new StoreEmail(fxCon.getUrlValue(),
                        fxCon.getDbUserNameValue(),
                        fxCon.getDbUserPassValue());
                try {
                    for (EmailBean received : listReceived) {
                        store.storeEmail(received, "Received");
                    }
                } catch (IOException ex) {
                    throw ex;
                }
            }
        } catch (SQLException | IOException ex) {
            throw ex;

        }
    }
}
