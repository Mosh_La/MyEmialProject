/**
 * This class represents the window to send messages
 */
package com.mez.myEmailProject.controller;

import com.mez.myEmailProject.fxData.FXAttachBean;
import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.fxData.FXEmailBean;
import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.business.SendEmail;
import com.mez.myEmailProject.data.EmailBean;
import com.mez.myEmailProject.manager.PropertiesManager;
import com.mez.myEmailProject.persistence.RestoreEmail;
import com.mez.myEmailProject.persistence.StoreEmail;
import com.mez.myEmailProject.persistence.UpdateDAO;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendFXMLController implements ControllerInterface {

    private Stage stage;
    private MainFXMLController main;
    private final Util util;
    private List<FXAttachBean> regularAttach;
    private List<FXAttachBean> embeddedAttach;
    private FXConfig fxConfig;
    private FXEmailBean fxEmailBean;
    private FXMLLoader loader;
    private final static Logger LOG = LoggerFactory.getLogger(SendFXMLController.class);

    /**
     * Init constructor for: #Controllers #FXbeans that will be set up later in
     * the code #Util, a helper class
     */
    public SendFXMLController() {
        util = new Util();
        regularAttach = new ArrayList();
        embeddedAttach = new ArrayList();
        fxConfig = new FXConfig();
        fxEmailBean = new FXEmailBean();
    }

    @FXML
    private HTMLEditor html;

    @FXML
    private TextField to;

    @FXML
    private TextField cc;

    @FXML
    private TextField bcc;

    @FXML
    private HBox container;

    @FXML
    private TextField subject;

    @FXML
    private TextArea plainText;

    /**
     * Adds an attachment to the attachment list
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void addAttachment(ActionEvent event) throws IOException {
        LOG.info("Add regular attachment button was cliked!");
        choseFiles(false);
    }

    /**
     * Adds an embedded attachment to the embedded_attachment list
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void addEmbeddedAttach(ActionEvent event) throws IOException {
        LOG.info("Add embedded button was cliked!");
        choseFiles(true);
    }

    /**
     * Private helper method to create an attachment btn
     */
    private void createBtn(FXAttachBean attach, HBox hbox) {
        Button attachBtn = new Button();
        attachBtn.setId("btnAttach");
        attachBtn.setText(attach.getAttachmentNameValue());
        attachBtn.setOnAction(e -> remove(attachBtn, attach));
        hbox.getChildren().addAll(attachBtn);
        hbox.setSpacing(5);
    }

    /**
     * Generic helper method that will add any type of attachment to its
     * appropriate list
     *
     * @param isEmbedded
     * @throws IOException
     */
    private void choseFiles(boolean isEmbedded) throws IOException {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null); //by default
        if (selectedFile != null && !isEmbedded) {
            //add it to the list
            FXAttachBean attach = new FXAttachBean(selectedFile, false);
            regularAttach.add(attach);
            createBtn(attach, container);

        } else if (selectedFile != null) {
            String path = selectedFile.getPath();
            html.setHtmlText(
                    "<img src=\"" + path
                    + "\" width=\"32\" height=\"32\" >"
            );
            LOG.info(path);

            embeddedAttach.add(new FXAttachBean(selectedFile, true));
            LOG.info("A new attachmentBean has been created! -> there are " + embeddedAttach.size() + " embedded attachments in total!");
        }
    }

    /**
     * Initialize constructor
     *
     * @throws IOException
     */
    public void initialize() throws IOException {
        LOG.info("Called when program start up");
        PropertiesManager pm = new PropertiesManager();
        pm.loadTextProperties(fxConfig, "", "MailConfig");
        fxEmailBean.setUserNameProperty(fxConfig.getUserNameValue());
        fxEmailBean.setSenderAddressProperty(fxConfig.getUserEmailAddressValue());
        fxEmailBean.setSenderPassProperty(fxConfig.getUserPassValue());
        bindParams();
    }

    /**
     * Helper method to bind param
     */
    private void bindParams() {
        Bindings.bindBidirectional(subject.textProperty(), fxEmailBean.getSubjectProperty());
        Bindings.bindBidirectional(to.textProperty(), fxEmailBean.getToAddressProperty());
        Bindings.bindBidirectional(cc.textProperty(), fxEmailBean.getCcProperty());
        Bindings.bindBidirectional(bcc.textProperty(), fxEmailBean.getBccProperty());
        Bindings.bindBidirectional(plainText.textProperty(), fxEmailBean.getPlainTextMessageProperty());
    }

    /**
     * Private helper method that helps remove selected attachment from the
     * display to the user and from the list of regular attachments
     *
     * @param attach
     */
    private void remove(Button attach, FXAttachBean fxBean) {
        //util.alertInfo("Are you sure you want to delete?", "Delete?");
        if (util.alertWithButtons("Delete", "Are you sure you want to delete this attachment?",
                "Yes", "No")) {
            regularAttach.remove(fxBean);// --> phase4
            container.getChildren().remove(attach);
            LOG.info("An attachmentBean has been removed");
        }
    }

    /**
     * The current email will be saved to the drat folder and will be editable
     * (phase4)
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void saveToDraft(ActionEvent event) throws IOException, SQLException {
        fxEmailBean.setAttachmentProperty(regularAttach);
        fxEmailBean.setEmbeddedAttachmentProperty(embeddedAttach);
        fxEmailBean.setHtmlMessageProperty(html.getHtmlText());
        EmailBean email = new EmailBean(fxEmailBean);
        LOG.debug(fxEmailBean.getIdValue() + "");
        this.fxConfig = util.getFXConfig();
        StoreEmail store = new StoreEmail(fxConfig.getUrlValue(),
                fxConfig.getDbUserNameValue(),
                fxConfig.getDbUserPassValue());
        RestoreEmail restore = new RestoreEmail(fxConfig.getUrlValue(),
                fxConfig.getDbUserNameValue(),
                fxConfig.getDbUserPassValue());
        //check if doesn't exist in draft
        //if id is equal to -1, the email is not stored in the database
        if (email.getId() == -1) {
            //the email has not been stored in draft
            store.storeEmail(email, "Draft");
            util.alertInfo("Your message saved in Draft", "Info");
            goBackToMain(event);
        } else {
            UpdateDAO update = new UpdateDAO(fxConfig.getUrlValue(),
                    fxConfig.getDbUserNameValue(),
                    fxConfig.getDbUserPassValue());
            update.updateDraft(fxEmailBean, fxEmailBean.getIdValue());
            util.alertInfo("Your message has been saved and modified in Draft", "Info");
            goBackToMain(event);

        }
    }

    /**
     * This method will save the email in the database and will send it to the
     * recepiante a combination of phase1 + phase2! will be fully implemented in
     * phase4
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void sendEmail(ActionEvent event) throws IOException, SQLException {
        fxEmailBean.setAttachmentProperty(regularAttach);
        fxEmailBean.setEmbeddedAttachmentProperty(embeddedAttach);
        fxEmailBean.setHtmlMessageProperty(html.getHtmlText());
        EmailBean email = new EmailBean(fxEmailBean);
        this.fxConfig = util.getFXConfig();
        SendEmail sendEmail = new SendEmail(email, fxConfig.getSmtpServerValue());
        StoreEmail store = new StoreEmail(fxConfig.getUrlValue(),
                fxConfig.getDbUserNameValue(),
                fxConfig.getDbUserPassValue());
        LOG.debug(fxConfig.getUrlValue() + "\n" +
                fxConfig.getDbUserNameValue() + "\n" +
                fxConfig.getDbUserPassValue());
        try {
            sendEmail.sendEmail();
            LOG.info("An email was sent with id -> " + email.getId());
            store.storeEmail(email, "Sent");
            util.alertInfo("Your message is sent! :)", "Info");
            goBackToMain(event);
        } catch (IllegalArgumentException iae) {
            util.alertError(iae.getMessage(), "Oops, there was a problem");
        }

    }

    /**
     * This method will set the scene to the main page
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void goBackToMain(ActionEvent event) throws IOException, SQLException {
        LOG.info("back button was cliked! -> going back to main");
        loader = new FXMLLoader(getClass().getResource("/fxml/MainPage.fxml"));
        Parent root = loader.load();
        main = loader.getController();
        main.receiveEmails();
        main.getFolders();
        main.showFirstElementInFolderList();
        main.setStage(stage);
        stage.setScene(new Scene(root));
        stage.setTitle("Welcome - Main Page");
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Overriding the method from the interface to keep track of the current
     * stage
     *
     * @param stage
     */
    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Receives an email to edit, and will populate the fields
     *
     * @param bean
     */
    public void editDraft(FXEmailBean bean) {
        fxEmailBean = bean;
        LOG.debug(bean.getIdValue() + "");
        bindParams();
        for (FXAttachBean regAttach : bean.getAttachmentValue()) {
            this.createBtn(regAttach, container);
        }
        html.setHtmlText(bean.getHtmlMessageValue());
        //embedded todo
        //text todo
    }

    /**
     * 'Reply' sends your response only to the person that sent you the mail.
     *
     * @param bean
     */
    public void reply(FXEmailBean bean) {
        to.setText(bean.getSenderAddressValue());
    }

    /**
     * 'Reply To All' sends your response to everyone the mail was sent to or
     * were Cc'd.
     *
     * @param bean
     */
    public void replyAll(FXEmailBean bean) {
        to.setText(bean.getToAddressValue());
        cc.setText(bean.getCcValue());
    }

    /**
     * "Forward" sends the message to another person or group, and will include
     * any attachments included in the original email.
     *
     * @param bean
     */
    public void forward(FXEmailBean bean) {
        html.setHtmlText(bean.getHtmlMessageValue());
        to.setText(bean.getToAddressValue());
        for (FXAttachBean regAttach : bean.getAttachmentValue()) {
            this.createBtn(regAttach, container);
        }
        subject.setText("Fwd:" + bean.getSubjectValue());
        //embedded todo
    }
}
