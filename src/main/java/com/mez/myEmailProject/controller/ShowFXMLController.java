/*
 * * This class represents simply shows an email that was clicked on at the main page 
 */
package com.mez.myEmailProject.controller;

import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.fxData.FXAttachBean;
import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.fxData.FXEmailBean;
import com.mez.myEmailProject.persistence.UpdateDAO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1631492
 */
public class ShowFXMLController implements ControllerInterface {

    private final Util util = new Util();
    private final static Logger LOG = LoggerFactory.getLogger(ShowFXMLController.class);

    private Stage stage;
    private MainFXMLController main;

    @FXML
    private AnchorPane ccField;

    @FXML
    private AnchorPane toField;

    @FXML
    private Label subject;

    @FXML
    private WebView textHtmlAndAttach;

    @FXML
    private AnchorPane attachments;
    @FXML
    private Text simpleText;

    private FXEmailBean currentFXBean;
    private List<String> listOfFolders;

    /**
     * Will go back to the main page
     *
     * @param event
     * @throws IOException
     * @throws SQLException
     */
    @FXML
    void goBackToMain(ActionEvent event) throws IOException, SQLException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainPage.fxml"));
        Parent root = loader.load();
        main = loader.getController();
        main.setStage(stage);
        main.receiveEmails();
        main.getFolders();
        main.showFirstElementInFolderList();
        stage.setScene(new Scene(root));
        stage.setTitle("Welcome - Main Page");
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Overriding the method from the interface to keep track of the current
     * stage
     *
     * @param s
     */
    @Override
    public void setStage(Stage s) {
        this.stage = s;
    }

    /**
     * Will show the selected email from the table
     *
     * @param bean
     */
    public void showEmail(FXEmailBean bean, List<String> list) {
        this.currentFXBean = bean;
        this.listOfFolders = list;
        //to     
        Label to = new Label();
        to.setText(bean.getToAddressValue());
        toField.getChildren().addAll(to);
        //cc
        Label cc = new Label();
        cc.setText(bean.getCcValue());
        ccField.getChildren().addAll(cc);
        //htmltext
        textHtmlAndAttach.getEngine().loadContent(bean.getHtmlMessageValue());
        simpleText.setText(bean.getPlainTextMessageValue());
        //subject
        subject.setText(bean.getSubjectValue());
        //attachment -> create buttons that represents the attachments - add style
        HBox container = new HBox();
        container.setSpacing(5);
        for (FXAttachBean a : bean.getAttachmentValue()) {
            Button attachBtn = new Button();
            File f = new File(a.getFilePathValue());
            LOG.info("A file to represent an attach was created!" + f.toString());
            attachBtn.setText(a.getAttachmentNameValue());
            attachBtn.setId("btnAttach");
            attachBtn.setOnAction(e -> download(a.getAttachmentNameValue(), a.getAttachmentByteValue()));
            container.getChildren().add(attachBtn);
        }
        attachments.getChildren().add(container);
    }

    /**
     * Gives the user the ability to chose where to save to the attachment
     *
     * @param name
     * @param content
     */
    public void download(String name, byte[] content) {
        try {
            DirectoryChooser dirChooser = new DirectoryChooser();
            dirChooser.setTitle("Save Image to PC");
            File chosenDir = dirChooser.showDialog(new Stage());
            File newFile = new File(chosenDir.getPath() + "\\" + name);
            try (FileOutputStream stream = new FileOutputStream(newFile.getPath())) {
                stream.write(content);
                util.alertInfo("Image has been succesfully saved into ->" + chosenDir.getPath(), "Succesfully Saved!");
            } catch (FileNotFoundException ex) {
                LOG.error(ex.getMessage());
            } catch (IOException ex) {
                LOG.error(ex.getMessage());
            }
        } catch (NullPointerException ex) {
            ex.getMessage();
        }
    }

    /**
     * Will place the email in a given folder
     *
     * @param event
     */
    @FXML
    void moveToAnotherFolder(ActionEvent event) throws IOException, SQLException {
        listOfFolders.remove("Draft"); //because you can't store read-only in draft which is editable
        ChoiceDialog<String> dialog = new ChoiceDialog<>(listOfFolders.get(0), listOfFolders);
        dialog.setTitle("Move to another folder");
        dialog.setContentText("Chose the folder you want to move to");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            //update
            FXConfig fxCon = util.getFXConfig();
            if (!fxCon.getUserNameValue().equals("")) {
                UpdateDAO update = new UpdateDAO(fxCon.getUrlValue(),
                        fxCon.getDbUserNameValue(),
                        fxCon.getDbUserPassValue());
                update.moveToAnotherFolder(result.get(), currentFXBean.getIdValue());
                util.alertInfo("Email has been move to " + result.get(), "Success");
                goBackToMain(event);
            }

        }
    }
}
