/*
 * Interface that garantees that a controller will have a setStage method
 */
package com.mez.myEmailProject.controller;

import javafx.stage.Stage;

/**
 *
 * @author 1631492 Lara
 */
public interface ControllerInterface {

/**
 * Set current stage in controller
 * @param s 
 */    
public void setStage(Stage s);
}
