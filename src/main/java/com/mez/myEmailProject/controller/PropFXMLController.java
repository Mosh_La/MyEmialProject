/**
 * This class a layout where the user will have to fill in his info
 * This layout will pop up only once as it keeps the user signed in
 * */
package com.mez.myEmailProject.controller;

import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.manager.PropertiesManager;
import java.io.IOException;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.beans.binding.Bindings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropFXMLController {

    private Scene scene;
    private Stage stage;
    private MainFXMLController main;
    private final Util util = new Util();
    private final FXConfig config;
    private final static Logger LOG = LoggerFactory.getLogger(MainFXMLController.class);

    @FXML
    private TextField userName;

    @FXML
    private TextField userPass;

    @FXML
    private TextField userEmail;

    @FXML
    private TextField imapServer;

    @FXML
    private TextField smtpServer;

    @FXML
    private TextField imapPort;

    @FXML
    private TextField smtpPort;

    @FXML
    private TextField dbName;

    @FXML
    private TextField dbPort;

    @FXML
    private TextField dbUserName;

    @FXML
    private TextField dbUserPass;

    @FXML
    private TextField dbUrl;

    @FXML
    private Button signInBtn;

    @FXML
    private Button clear;

    /**
     * Default constructor creates an instance of FishData that can be bound to
     * the form
     */
    public PropFXMLController() {
        super();
        config = new FXConfig();
    }

    /**
     * Without the ability to pass values thru a constructor we need a set
     * method for any variables required in this class
     *
     * @param scene
     * @param stage
     * @param main
     */
    public void setSceneStageController(Scene scene, Stage stage, MainFXMLController main) {
        this.scene = scene;
        this.stage = stage;
        this.main = main;
    }

    /**
     * The initialize method is used to handle bindings of FXbeans to dataBeans
     */
    @FXML
    //ask the teacher why this works
    public void initialize() {
        LOG.info("This should be printed once, and it it does, theres some spooooky");
        Bindings.bindBidirectional(userName.textProperty(), config.getUserNameProperty());
        Bindings.bindBidirectional(userEmail.textProperty(), config.getUserEmailAddressProperty());
        Bindings.bindBidirectional(userPass.textProperty(), config.getUserPassProperty());

        Bindings.bindBidirectional(imapServer.textProperty(), config.getImapServerProperty());
        Bindings.bindBidirectional(smtpServer.textProperty(), config.getSmtpServerProperty());
        Bindings.bindBidirectional(imapPort.textProperty(), config.getImapPortProperty());
        Bindings.bindBidirectional(smtpPort.textProperty(), config.getSmtpPortProperty());

        Bindings.bindBidirectional(dbName.textProperty(), config.getDbNameProperty());
        Bindings.bindBidirectional(dbPort.textProperty(), config.getDbPortProperty());
        Bindings.bindBidirectional(dbUrl.textProperty(), config.getUrlProperty());

        Bindings.bindBidirectional(dbUserName.textProperty(), config.getDbUserNameProperty());
        Bindings.bindBidirectional(dbUserPass.textProperty(), config.getDbUserPassProperty());
        LOG.info(("All JAVAFX was binded with regular bean"));

    }

    /**
     * Event handler for Cancel button Exit the program
     */
    @FXML
    void onCancel(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Resets the form, will set all the values to ""
     *
     * @param event
     */
    @FXML
    void clearAll(ActionEvent event) {
        config.setUserNameProperty("");
        config.setUserPassProperty("");
        config.setUserEmailAddressProperty("");

        config.setDbNameProperty("");
        config.setDbPortProperty("");
        config.setUrlProperty("");

        config.setSmtpServerProperty("");
        config.setSmtpPortProperty("");
        config.setImapServerProperty("");
        config.setImapPortProperty("");

        config.setDbUserNameProperty("");
        config.setDbUserPassProperty("");
        LOG.info("All properties were set to empty strings");
    }

    /**
     * Creates properties file on click of "Sign-in buttons" Will validate if
     * properties are not ""
     *
     * @param event
     */
    @FXML
    void createPropFile(ActionEvent event) throws SQLException {
        PropertiesManager pm = new PropertiesManager();
        try {
            if (pm.writeTextProperties("", "MailConfig", config)) { // properties are valid if method returns true
                main.receiveEmails();
                main.getFolders();
                main.showFirstElementInFolderList();
                main.setStage(stage);
                stage.setScene(scene);
                LOG.info("Properties file was succesfully created :) Welcome dear user!");

            } else {
                util.alertError("You entered wrong data, file was not created, try again!", "Error!");
                LOG.info("File is not created because user entered wrong info");

            }
        } catch (IOException ex) {
            util.alertError("Coudl't find file in this path", "Error!");
            LOG.info("An exception occured while searching for the path");
            Platform.exit();
        }
    }
}
