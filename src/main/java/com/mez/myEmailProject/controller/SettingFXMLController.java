/**
 * This class represents a setting helper page
 */
package com.mez.myEmailProject.controller;

import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.manager.PropertiesManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SettingFXMLController implements ControllerInterface {

    private Stage stage;
    private final Util util = new Util();
    private final FXConfig config = new FXConfig();
    private MainFXMLController main;

    @FXML
    private Label userNameText;

    @FXML
    private Label userNameText1;

    @FXML
    private Label passwordText;

    @FXML
    private Label emailText;

    @FXML
    private Label imapServerText;

    @FXML
    private Label smtpServerText;

    @FXML
    private Label imapPortText;

    @FXML
    private Label smtpPortText;

    @FXML
    private Label dbNameText;

    @FXML
    private Label dbPortText;

    @FXML
    private Label dbUserNameText;

    @FXML
    private Label dbUserPassText;

    @FXML
    private Label dbUrlText;

    private final static Logger LOG = LoggerFactory.getLogger(MainFXMLController.class);

    /**
     * This method will set the scene to the main page
     *
     * @param event
     * @throws IOException
     */
    @FXML
    void goBackToMain(ActionEvent event) throws IOException, SQLException {
        LOG.info("back button was cliked! -> going back to main");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainPage.fxml"));
        Parent root = loader.load();
        main = loader.getController();
        main.receiveEmails();
        main.getFolders();
        main.showFirstElementInFolderList();
        main.setStage(stage);
        stage.setScene(new Scene(root));
        stage.setTitle("Welcome - Main Page");
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Overriding the method from the interface to keep track of the current
     * stage
     *
     * @param stage
     */
    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Will change the property file and the display in the setting page if the
     * user clicks on the menu and chooses what property he wants to modify
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void changeProperty(ActionEvent event) throws IOException {
        String id = ((MenuItem) event.getSource()).getId();
        try {
            String newValue = util.alertWithButtonsWithInput("Change Property", "Add new value", "OK");
            PropertiesManager pm = new PropertiesManager();
            if (pm.loadTextProperties(config, "", "MailConfig")) {

                if (id.equals("userName")) {
                    config.setUserNameProperty(newValue);

                }
                if (id.equals("userPassword")) {
                    config.setUserPassProperty(newValue);
                }

                if (id.equals("userEmailAddress")) {
                    config.setUserEmailAddressProperty(newValue);
                }
                if (id.equals("imapServer")) {
                    config.setImapServerProperty(newValue);
                }

                if (id.equals("smtpServer")) {
                    config.setSmtpServerProperty(newValue);
                }

                if (id.equals("imapPort")) {
                    config.setImapPortProperty(newValue);
                }
                if (id.equals("smtpPort")) {
                    config.setSmtpPortProperty(newValue);
                }
                if (id.equals("dbName")) {
                    config.setDbNameProperty(newValue);
                }

                if (id.equals("dbPort")) {
                    config.setDbPortProperty(newValue);
                }

                if (id.equals("dbUserName")) {
                    config.setDbUserNameProperty(newValue);
                }
                if (id.equals("dbUserPassword")) {
                    config.setDbUserPassProperty(newValue);
                }

                if (id.equals("dbURL")) {
                    config.setUrlProperty(newValue);
                }
            }
            pm.writeTextProperties("", "MailConfig", config);
            dipslayProperties();
            LOG.info("Properties file has been modified with a new value of " + id + " is now -->" + newValue);
        } catch (NoSuchElementException ex) {
            ex.getMessage();

        }

    }

    /**
     * Will display properties from the MailConfig file in the Setting page
     *
     * @throws IOException
     */
    public void dipslayProperties() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        pm.loadTextProperties(config, "", "MailConfig");
        userNameText.setText(config.getUserNameValue());
        userNameText1.setText(config.getUserNameValue());
        passwordText.setText(config.getUserPassValue());
        emailText.setText(config.getUserEmailAddressValue());
        imapServerText.setText(config.getImapServerValue());
        smtpServerText.setText(config.getSmtpServerValue());
        imapPortText.setText(config.getImapPortValue());
        smtpPortText.setText(config.getSmtpPortValue());
        dbNameText.setText(config.getDbNameValue());
        dbPortText.setText(config.getDbPortValue());
        dbUserNameText.setText(config.getDbUserNameValue());
        dbUserPassText.setText(config.getDbUserPassValue());
        dbUrlText.setText(config.getUrlValue());
        LOG.info("Properties are now displayed to the user in the Setting page");
    }

    /**
     * Will initialize all the properties from the MailConfig file and display
     * them on the Setting page
     *
     * @throws IOException
     */
    @FXML
    void initialize() throws IOException {
        dipslayProperties();
    }

    /**
     * Open about dialog to provide the user with more information about the app
     * Has a link to an assistance video I created
     *
     * @param event
     */
    @FXML
    void openAbout(ActionEvent event) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Welcome to my Dawson Emaling System! - About The App");
        alert.setContentText("Release date: 06/11/18 \n"
                + "**Developers**: Lara Mezirovsky™ \n"
                + "TIPS: any changes in the database require a refresh, click again on the folder you are currently in"
                + "or any other folder to see the changes!"
                + "\n"
                + "**Version: 1.0** \n"
                + "For more information: consult the readme file attached to this project or"
                + "visit the following link: https://drive.google.com/open?id=19ecXXwGSMbv6gCv8HTof2I9JDufFFcA0");
        alert.showAndWait();
    }
}
