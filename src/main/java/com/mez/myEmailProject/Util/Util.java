package com.mez.myEmailProject.Util;

import com.mez.myEmailProject.fxData.FXConfig;
import com.mez.myEmailProject.controller.ControllerInterface;
import com.mez.myEmailProject.manager.PropertiesManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;

/*
 * @author 1631492 Lara
 */
public class Util {

    public final static org.slf4j.Logger LOG = LoggerFactory.getLogger(Util.class);

    /**
     * Generic popup that shows an info popup dialog
     *
     * @param msg
     * @param title
     */
    public void alertInfo(String msg, String title) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle(title);
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Generic popup that shows an error popup dialog
     *
     * @param msg
     * @param title
     */
    public void alertError(String msg, String title) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(title);
        dialog.setContentText(msg);
        dialog.show();
    }

    /**
     * Generic popup that lets the user choose between button1 and button2
     *
     * @param title
     * @param message
     * @param option1
     * @param option2
     * @return
     */
    public boolean alertWithButtons(String title, String message, String option1, String option2) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setContentText(message);
        ButtonType btn1 = new ButtonType(option1);
        ButtonType btn2 = new ButtonType(option2);
        alert.getButtonTypes().setAll(btn1, btn2);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == btn1;

    }

    /**
     * Generic popup that accepts user input
     *
     * @param title
     * @param message
     * @param option1
     * @return
     */
    public String alertWithButtonsWithInput(String title, String message, String option1) {
        TextInputDialog text = new TextInputDialog();
        text.setTitle(title);
        text.setContentText(message);
        Optional<String> result = text.showAndWait();
        return result.get();
    }

    /**
     * This is a helper method that will help reduce repetitive code when
     * changing the scene within a given stage
     *
     * @param stage
     * @param controller
     * @param title
     * @param fxmlPath
     * @return
     * @throws java.io.IOException
     */
    public Stage createSceneToGivenStage(Stage stage, ControllerInterface controller,
            String title, String fxmlPath) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlPath));
        Parent root = loader.load();
        controller = loader.getController();
        controller.setStage(stage);
        stage.setScene(new Scene(root));
        stage.setTitle(title);
        stage.setResizable(false);
        return stage;
    }

    /**
     * This method accepts a file and searches for its path to convert the file
     * into a byte[]
     *
     * @param f
     * @return converted Byte Array
     * @throws IOException
     */
    public byte[] convertFileToByteArray(File f) throws IOException {
        return Files.readAllBytes(f.toPath());
    }

    /**
     * Helper method that returns a FXConfig object from the properties file
     *
     * @return
     * @throws IOException
     */
    public FXConfig getFXConfig() throws IOException {
        PropertiesManager pm = new PropertiesManager();
        FXConfig fxCon = new FXConfig();
        if(checkForProperties()){
        pm.loadTextProperties(fxCon, "", "MailConfig");
        }
        return fxCon;
    }
    
    /**
     * Checks if properties file exists
     * @return 
     */
     private boolean checkForProperties() {
        boolean found = false;
        FXConfig conf = new FXConfig();
        PropertiesManager pm = new PropertiesManager();

        try {
            if (pm.loadTextProperties(conf, "", "MailConfig")) {
                found = true;
            }
        } catch (IOException ex) {
            LOG.error("checkForProperties error", ex);
        }
        return found;
    }
    
}
