package com.mez.myEmailProject.business;

import com.mez.myEmailProject.data.AttachmentBean;
import com.mez.myEmailProject.data.EmailBean;

import java.io.IOException;
import java.util.List;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 1631492 Lara
 * @version 1.0
 */
public class SendEmail {

    private SmtpServer smtpServer;
    private final String senderEmail, senderUserName, senderPass, subject,
            plainTextMessage, htmlMessage, smtpServerName;
    private final List<String> receiver, cc, bcc;
    private final List<AttachmentBean> listOfAttachments, listOfEmbeddedAttachments;
    private final static Logger LOG = LoggerFactory.getLogger(SendEmail.class);

    /**
     * Initialization constructor
     *
     * @param emailBean
     * @param smtpServer
     */
    public SendEmail(EmailBean emailBean, String smtpServer) {
        this.senderUserName = emailBean.getUserName();
        this.senderEmail = emailBean.getSenderAddress();
        this.senderPass = emailBean.getSenderPass();
        this.receiver = emailBean.getToAddress();
        this.cc = emailBean.getCc();
        this.bcc = emailBean.getBcc();
        this.subject = emailBean.getSubject();
        this.plainTextMessage = emailBean.getPlainTextMessage();
        this.htmlMessage = emailBean.getHtmlMessage();
        this.listOfAttachments = emailBean.getAttachment();
        this.listOfEmbeddedAttachments = emailBean.getEmbeddedAttachment();
        this.smtpServerName = smtpServer;
    }

    /**
     * This method sends an email Possible email components: attachment, bcc, cc
     * Mandatory email components: to, from Allowed to be empty components:
     * txtMessage, htmlMessage, subject etc..
     */
    public void sendEmail() {
        //validate each possible receiver, if invalid, will be deleted from the list
        if (!this.cc.isEmpty()) {
            validateReceiverEmail(this.cc);
        }
        if (!this.bcc.isEmpty()) {
            validateReceiverEmail(this.bcc);
        }
        if (!this.receiver.isEmpty()) {
            validateReceiverEmail(this.receiver);
        }
        //validate if there is at least ONE receiver of any type (to, bcc, from) left after the validation
        if (checkEmail(this.senderEmail)
                && (!(this.receiver.isEmpty())
                || (!(this.cc.isEmpty()))
                || (!(this.bcc.isEmpty())))) {
            createServer();
            sendMailWithASession(createEmailObject());
        } else {
            throw new IllegalArgumentException("Must have at least ONE valid receiver");
        }
    }

    /**
     * A private helper method that helps create a SendMailSession object
     *
     * @param email
     */
    private void sendMailWithASession(Email email) {
        SendMailSession session = this.smtpServer.createSession();
        session.open();
        session.sendMail(email);
    }

    /**
     * Helper method that helps create a smptServer to send emails.
     */
    private void createServer() {
        this.smtpServer = MailServer.create()
                .ssl(true)
                .host(this.smtpServerName)
                .auth(this.senderEmail, this.senderPass)
                .debugMode(true)
                .buildSmtpMailServer();
    }

    /**
     * A helper method that will validate each receiver in the list. Will loop
     * even if only one receiver
     */
    private void validateReceiverEmail(List<String> receiver) {
        for (int index = 0; index < receiver.size(); index++) {
            if (!(checkEmail(receiver.get(index)))) {
                receiver.remove(index);
            }
        }
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @author Kenneth Fogel
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    /**
     * A private method that helps create a JoddObject.
     *
     * @return Email of type Jodd
     * @throws IOException
     */
    private Email createEmailObject() {
        //add the basic components of an email
        Email email = Email.create()
                .from(this.senderUserName, this.senderEmail)
                .to((receiver.toArray(new String[receiver.size()])))
                .subject(this.subject)
                .textMessage(this.plainTextMessage)
                .htmlMessage(this.htmlMessage);
        //add additional components -> list size may be 0
        if (!(this.cc.isEmpty())) {
            email.cc(cc.toArray(new String[cc.size()]));
        }

        if (!(this.bcc.isEmpty())) {
            email.bcc(bcc.toArray(new String[bcc.size()]));
        }

        listOfAttachments.forEach((attachment) -> {
            email.attachment(EmailAttachment.with().content(attachment.getFile()));
        });

        listOfEmbeddedAttachments.forEach((embeddedAttachment) -> {
            email.embeddedAttachment(EmailAttachment.with().content(embeddedAttachment.getFile()));
        });

        return email;
    }
}
