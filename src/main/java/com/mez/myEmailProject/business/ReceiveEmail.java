package com.mez.myEmailProject.business;

import com.mez.myEmailProject.data.AttachmentBean;
import com.mez.myEmailProject.data.EmailBean;
import java.util.ArrayList;
import java.util.List;
import javax.mail.Flags;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailFilter;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 1631492 Lara
 * @version 1.0
 */
public class ReceiveEmail {

    private String receiver, receiverPass, imapServerName;
    private ImapServer imapServer;
    private ReceiveMailSession session;
        private final static Logger LOG = LoggerFactory.getLogger(ReceiveEmail.class);


    /**
     *
     * @param email
     * @param pass
     * @param imapServerName
     */
    public ReceiveEmail(String email, String pass, String imapServerName) { //and imap server in phase4
        this.receiver = email;
        this.receiverPass = pass;
        this.imapServerName = imapServerName;
    }

    /**
     * Receives a list of emails using the IMAP server
     *
     * @return
     */
    public List<EmailBean> receiveEmail() {
        List<EmailBean> receivedEmailList = new ArrayList<>();
        if (validateEmail(receiver)) {
            if (!(receiver.isEmpty())) { 
                createIMAPServer();
                session = imapServer.createSession();
                session.open();
                ReceivedEmail[] emails = this.session.receiveEmailAndMarkSeen(EmailFilter
                        .filter().flag(Flags.Flag.SEEN, false));
                receivedEmailList = acceptEmails(emails, receivedEmailList);
                session.close();
            }
            LOG.info(receivedEmailList.size() + " -> there were emails received******");
            return receivedEmailList;
        }
        return receivedEmailList;
    }

    /**
     * Will create a list from the emails that were received and not read
     *
     * @param emails
     * @param list
     * @return
     */
    private List<EmailBean> acceptEmails(ReceivedEmail[] emails, List<EmailBean> list) {
        if (emails != null) {
            for (ReceivedEmail email : emails) {
                EmailBean emailBean = new EmailBean();
                emailBean = createEmail(email, emailBean);
                List<EmailAttachment<? extends javax.activation.DataSource>> attachments = email.attachments();
                List<AttachmentBean> embedded = new ArrayList<>(0);
                List<AttachmentBean> regular = new ArrayList<>(0);
                for (EmailAttachment attachment : attachments) {
                    if (attachments != null) {
                        AttachmentBean attach = createAttachment(attachment);
                        if (attachment.isEmbedded()) {
                            embedded.add(attach);
                        } else {
                            regular.add(attach);
                        }
                    }
                }
                emailBean.setAttachment(regular);
                emailBean.setEmbeddedAttachment(embedded);
                list.add(emailBean);
            }
        }
        return list;
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @author Kenneth Fogel
     * @param address
     * @return true is OK, false if not
     */
    private boolean validateEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    /**
     * Private helper method to generate an object of type imapServer to receive
     * emails
     */
    private void createIMAPServer() {
        imapServer = MailServer.create()
                .host(this.imapServerName) 
                .ssl(true)
                .auth(this.receiver, this.receiverPass)
                .debugMode(true)
                .buildImapMailServer();
    }

    /**
     * Private helper method that generates an attachemntBean instance
     *
     * @return
     */
    private AttachmentBean createAttachment(EmailAttachment attachment) {
      
        AttachmentBean attach = new AttachmentBean(attachment.getName(),
                    attachment.toByteArray(),
                    "", //add the file path
                    attachment.isEmbedded());
        return attach;
    }

    /**
     * Private helper method to generate list of strings (in this case
     * represents emails)
     *
     * @return List<String>
     */
    private List<String> createStringList(EmailAddress[] emailAddressArray) {
        List<String> result = new ArrayList();
        for (EmailAddress emailAddress : emailAddressArray) {
            result.add(emailAddress.toString());
        }
        return result;
    }

    /**
     * Private helper method that generates an EmailBean object
     *
     * @return EmailBean
     * @param ReceviedEmail, emailBean
     */
    private EmailBean createEmail(ReceivedEmail email, EmailBean emailBean) {
        emailBean.setUserName(email.from().getPersonalName());
        emailBean.setSenderAddress(email.from().getEmail());
        emailBean.setToAddress(createStringList(email.to()));
        emailBean.setSubject(email.subject());
        htmlOrPlainText(email, emailBean);
        emailBean.setCc(createStringList(email.cc()));
        return emailBean;
    }

    /**
     * Private helper method to set htmlMessage or plainTxt
     *
     * @param email
     * @param emailBean
     */
    private void htmlOrPlainText(ReceivedEmail email, EmailBean emailBean) {
        email.messages().forEach((msg) -> {
            if (msg.getMimeType().equalsIgnoreCase(MimeTypes.MIME_TEXT_PLAIN)) {
                emailBean.setPlainTextMessage(msg.getContent());
            } else {
                emailBean.setHtmlMessage(msg.getContent());
            }
        });
    }
}
