/*
 * This class represents an Email that has all the basic components -->
 * such as message, name of the sender, from whom, to whom, cc, bcc, -->
 * attachments and a subject.
 * The email can either accept embedded html or plain text or both.
 * Some of the variables can be null, meaning they are NOT manditory -->
 * for example an attachment, bcc, cc, subject, embedded html etc..
 */
package com.mez.myEmailProject.data;

import com.mez.myEmailProject.fxData.FXAttachBean;
import com.mez.myEmailProject.fxData.FXEmailBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.LoggerFactory;

/**
 * @author 1631492
 * @version 1.0
 */
public class EmailBean implements Serializable {

    private int id;
    private String userName, senderAddress, senderPass, subject,
            plainTextMessage, htmlMessage;
    private List<String> toAddress, cc, bcc;
    private List<AttachmentBean> attachment, embeddedAttachment;
    public final static org.slf4j.Logger LOG = LoggerFactory.getLogger(EmailBean.class);

    /**
     * Email bean constructor that converts an fxBean -> emailBean
     *
     * @param fxEmail
     */
    public EmailBean(FXEmailBean fxEmail) {
        this();
        LOG.debug("this is the subject, I want to check that it is not null " + subject);
        this.id = fxEmail.getIdValue(); //only when stored in db will change
        this.userName = fxEmail.getUserNameValue();
        this.senderAddress = fxEmail.getSenderAddressValue();
        this.senderPass = fxEmail.getSenderPassValue();
        this.subject = fxEmail.getSubjectValue();
        this.plainTextMessage = fxEmail.getPlainTextMessageValue();
        this.htmlMessage = fxEmail.getHtmlMessageValue();
        this.toAddress = convertStringToList(fxEmail.getToAddressValue());
        this.cc = convertStringToList(fxEmail.getCcValue());
        this.bcc = convertStringToList(fxEmail.getBccValue());
        getAttachmentsFromFxBean(fxEmail);

    }

    /**
     * Overloaded constructor that takes all the field of an email bean
     *
     * @param id
     * @param userName
     * @param sender
     * @param password
     * @param receiver
     * @param sub
     * @param plainTextMsg
     * @param htmlMsg
     * @param cc
     * @param bcc
     * @param attach
     * @param embeddedAttach
     */
    public EmailBean(int id, String userName, String sender, String password, List<String> receiver, String sub, String plainTextMsg,
            String htmlMsg, List<String> cc, List<String> bcc, List<AttachmentBean> attach,
            List<AttachmentBean> embeddedAttach) {
        //initialize all the values
        this();
        this.userName = userName;
        this.senderAddress = sender;
        this.senderPass = password;
        this.toAddress = receiver;
        this.cc = cc;
        this.bcc = bcc;
        if (!("").equals(sub) || sub != null) {
            this.subject = sub; //else let it be "" 
        }

        if (!"".equals(plainTextMsg) || plainTextMsg != null) {
            this.plainTextMessage = plainTextMsg; //else let it be "" 
        }
        if (!"".equals(htmlMsg) || htmlMsg != null) {
            this.htmlMessage = htmlMsg; //else let it be "" 
        }
        this.attachment = attach;
        this.embeddedAttachment = embeddedAttach;
    }

    /**
     * Helper method to generate an FXEmailBean from the current instance of
     * EmailBean
     *
     * @return
     */
    public FXEmailBean createFxBean() {
        List<FXAttachBean> regAttachList = new ArrayList<>();
        List<FXAttachBean> embeddedAttachList = new ArrayList<>();

        for (AttachmentBean regAttach : this.getAttachment()) {
            regAttachList.add(new FXAttachBean(regAttach));
        }

        for (AttachmentBean embeddedAttach : this.getAttachment()) {
            embeddedAttachList.add(new FXAttachBean(embeddedAttach));
        }
        return new FXEmailBean(this.getId(), this.getUserName(),
                this.getSenderPass(), this.getSenderAddress(),
                Strings.join(this.getToAddress(), ','),
                Strings.join(this.getCc(), ','),
                Strings.join(this.getBcc(), ','),
                this.getSubject(),
                this.getPlainTextMessage(),
                this.getHtmlMessage(),
                regAttachList,
                embeddedAttachList
        );
    }

    /**
     * Helper method to allocate attachments from EmailBean to FxEmailBean
     *
     * @param fxBean
     */
    private void getAttachmentsFromFxBean(FXEmailBean fxBean) {
        AttachmentBean bean;
        for (FXAttachBean fxAttach : fxBean.getAttachmentValue()) {
            bean = new AttachmentBean(
                    fxAttach.getFile(), fxAttach.getIsEmbeddedBooleanValue());
            if (fxAttach.getIsEmbeddedBooleanValue()) {
                this.embeddedAttachment.add(bean);
            } else {
                this.attachment.add(bean);
            }
        }
    }

    /**
     * Private helper method to convert a col to a list
     *
     */
    private List<String> convertStringToList(String a) {
        return new ArrayList<>(Arrays.asList(a.trim().split(",")));
    }

    public EmailBean() {
        //initial value
        id = -1; //no yet stored in the database
        toAddress = new ArrayList<>(0);
        subject = "";
        plainTextMessage = "";
        htmlMessage = "";
        cc = new ArrayList<>(0);
        bcc = new ArrayList<>(0);
        attachment = new ArrayList<>(0); //cid 
        embeddedAttachment = new ArrayList<>(0);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getSenderAddress() {
        return this.senderAddress;
    }

    public String getSenderPass() {
        return this.senderPass;
    }

    public List<String> getToAddress() {
        return this.toAddress;
    }

    public String getPlainTextMessage() {
        return this.plainTextMessage;
    }

    public String getHtmlMessage() {
        return this.htmlMessage;
    }

    public String getSubject() {
        return this.subject;
    }

    public List<AttachmentBean> getAttachment() {
        return this.attachment;
    }

    public List<String> getCc() {
        return this.cc;
    }

    public List<String> getBcc() {
        return this.bcc;
    }

    public List<AttachmentBean> getEmbeddedAttachment() {
        return this.embeddedAttachment;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public void setToAddress(List<String> toAddress) {
        this.toAddress = toAddress;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setPlainTextMessage(String plainTextMessage) {
        this.plainTextMessage = plainTextMessage;
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage = htmlMessage;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }

    public void setAttachment(List<AttachmentBean> attachment) {
        this.attachment = attachment;
    }

    public void setEmbeddedAttachment(List<AttachmentBean> embeddedAttachment) {
        this.embeddedAttachment = embeddedAttachment;
    }

    public void setSenderPass(String senderPass) {
        this.senderPass = senderPass;
    }

    /**
     * The equals method was overriden so has to be the hashcode method
     *
     * @return int hash
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.senderAddress);
        hash = 89 * hash + Objects.hashCode(this.subject);
        hash = 89 * hash + Objects.hashCode(this.plainTextMessage);
        hash = 89 * hash + Objects.hashCode(this.htmlMessage);
        hash = 89 * hash + Objects.hashCode(this.toAddress);
        hash = 89 * hash + Objects.hashCode(this.cc);
        hash = 89 * hash + Objects.hashCode(this.attachment);
        hash = 89 * hash + Objects.hashCode(this.embeddedAttachment);
        return hash;
    }

    /**
     * This method compares an EmailBean with another EmailBean Equality is
     * determined when ALL of the fields of EmailBean are equal to another
     * emailBean
     *
     * @param obj
     * @return true emailBean is the same, otherwise, false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailBean other = (EmailBean) obj;
        if (!Objects.equals(this.senderAddress, other.senderAddress)) {
            return false;
        }
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        if (!Objects.equals(this.plainTextMessage, other.plainTextMessage)) {
            return false;
        }
        if (!Objects.equals(this.htmlMessage, other.htmlMessage)) {
            return false;
        }
        if (!Objects.equals(this.toAddress, other.toAddress)) {
            return false;
        }
        if (!Objects.equals(this.cc, other.cc)) {
            return false;
        }
        if (!Objects.equals(this.attachment, other.attachment)) {
            return false;
        }
        return Objects.equals(this.embeddedAttachment, other.embeddedAttachment);
    }
}//end Email
