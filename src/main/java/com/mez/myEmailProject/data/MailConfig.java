/*
 * This class represnts an individual who has a 
 * first name, last name, a unique email, and a password
 */
package com.mez.myEmailProject.data;

import java.io.Serializable;

/**
 * @author 1631492
 * @version 1.0
 */
public final class MailConfig implements Serializable {

    private String userName;
    private String emailAddress;
    private String password;

    private String imapServer;
    private String smtpServer;
    private String iPort;
    private String sPort;

    private String dbName;
    private String dbPort;
    private String url;
    private String dbPass;

    private String dbUserName;
    private String dbUserPass;

    /**
     * Default con.
     */
    public MailConfig() {
    }

    /**
     * A constructor that takes all the needed fields in the MailConfig
     * @param userName
     * @param eAddress
     * @param pass
     * @param imapServer
     * @param smptServer
     * @param sPort
     * @param iPort
     * @param dbName
     * @param dbPort
     * @param dbPass
     * @param dbUserName
     * @param dbUserPass
     * @param url 
     */
    public MailConfig(String userName,
            String eAddress,
            String pass,
            String imapServer,
            String smptServer,
            String sPort,
            String iPort,
            String dbName,
            String dbPort,
            String dbPass,
            String dbUserName,
            String dbUserPass,
            String url) {
        this.userName = userName;
        this.emailAddress = eAddress;
        this.password = pass;
        this.imapServer = imapServer;
        this.smtpServer = smptServer;
        this.iPort = iPort;
        this.sPort = sPort;
        this.dbPort = dbPort;
        this.dbName = dbName;
        this.dbPort = dbPort;
        this.dbUserName = dbUserName;
        this.dbUserPass = dbUserPass;
        this.url = url;
        this.dbPass = dbPass;
    }// end con

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setImapServer(String imapServer) {
        this.imapServer = imapServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public void setiPort(String iPort) {
        this.iPort = iPort;
    }

    public void setsPort(String sPort) {
        this.sPort = sPort;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }

    public void setDbUserName(String dbUserName) {
        this.dbUserName = dbUserName;
    }

    public void setDbUserPass(String dbUserPass) {
        this.dbUserPass = dbUserPass;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public String getImapServer() {
        return imapServer;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public String getiPort() {
        return iPort;
    }

    public String getsPort() {
        return sPort;
    }

    public String getDbName() {
        return dbName;
    }

    public String getDbPort() {
        return dbPort;
    }

    public String getUrl() {
        return url;
    }

    public String getDbPass() {
        return dbPass;
    }

    public String getDbUserName() {
        return dbUserName;
    }

    public String getDbUserPass() {
        return dbUserPass;
    }
}//end MailConfig
