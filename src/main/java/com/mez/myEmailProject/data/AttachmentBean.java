/*
 * This class represents an attachment stored in a byte array
 * Attachment type can be: pptx, doc, txt, png etc...
 */
package com.mez.myEmailProject.data;

import com.mez.myEmailProject.Util.Util;
import java.io.*;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 1631492
 * @version 1.0
 */
public class AttachmentBean implements Serializable {

    private String attachmentName;
    private byte[] attachment;
    private String filePath;
    private boolean isEmbedded;
    private final Util util = new Util(); //helper class
    private File f;

    public AttachmentBean() {
    }

    /**
     * Constructor to initialize image values
     *
     * @param file
     * @param isEmbedded
     */
    private final static Logger LOG = LoggerFactory.getLogger(AttachmentBean.class);

    public AttachmentBean(File file, boolean isEmbedded) {
        try {
            this.f = file;
            LOG.info("This is " + f);
            this.filePath = file.getPath();
            this.attachment = util.convertFileToByteArray(file);
            this.attachmentName = file.getName();
            this.isEmbedded = isEmbedded;
        } catch (IOException ioe) {
            ioe.getMessage();
        }
    }

    /**
     * create a file to send
     *
     * @return
     */
    public File getFile() {
        return this.f;
    }

    /**
     * Overloaded con
     *
     * @param array
     * @param isEmbedded
     * @param filePath
     * @param name
     */
    public AttachmentBean(String name, byte[] array, String filePath, boolean isEmbedded) {
        this.attachmentName = name;
        this.attachment = array;
        this.filePath = filePath;
        this.isEmbedded = isEmbedded;
        this.f = new File(filePath);
    }

    public boolean getIsEmbedded() {
        return this.isEmbedded;
    }

    public byte[] getAttachment() {
        return this.attachment;
    }

    public String getAttachmentName() {
        return this.attachmentName;
    }

    public String getAttachmentPath() {
        return this.filePath;
    }

    public void setAttachmentName(String attachName) {
        this.attachmentName = attachName;
    }

    public void setAttachment(byte[] attachment) {
        this.attachment = attachment;
    }

    public String getFilePath() {
        return filePath;
    }

    /**
     * Attachments are equal if their byte array is equal
     *
     * @param obj
     * @return true if the same, false if different
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentBean other = (AttachmentBean) obj;
        if (this.isEmbedded != other.isEmbedded) {
            return false;
        }
        return Arrays.equals(this.attachment, other.attachment);
    }

    /**
     * By overriding the equals method, we have to override the hashCode()
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Arrays.hashCode(this.attachment);
        return hash;
    }
}//end Attachment
