/*
 * This is a class that stores configurations per user
 */
package com.mez.myEmailProject.manager;

import com.mez.myEmailProject.fxData.FXConfig;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 1631492 Lara
 * @version 1.0
 */
public class PropertiesManager {

    /**
     * Updates a FXConfig object with the contents of the properties file
     *
     * @param mailConfig
     * @param path
     * @param propFileName
     * @return
     * @throws java.io.IOException
     */
    private final static Logger LOG = LoggerFactory.getLogger(PropertiesManager.class);

    public final boolean loadTextProperties(final FXConfig mailConfig, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();
        Path txtFile = get(path, propFileName + ".properties");
        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            mailConfig.setUserNameProperty(prop.getProperty("userName"));
            mailConfig.setUserPassProperty(prop.getProperty("userPassword"));
            mailConfig.setUserEmailAddressProperty(prop.getProperty("userEmailAddress"));

            mailConfig.setImapServerProperty(prop.getProperty("imapServer"));
            mailConfig.setSmtpServerProperty(prop.getProperty("smtpServer"));
            mailConfig.setImapPortProperty(prop.getProperty("imapPort"));
            mailConfig.setSmtpPortProperty(prop.getProperty("smtpPort"));

            mailConfig.setDbNameProperty(prop.getProperty("dbName"));
            mailConfig.setDbPortProperty(prop.getProperty("dbPort"));
            mailConfig.setUrlProperty(prop.getProperty("url"));

            mailConfig.setDbUserNameProperty(prop.getProperty("dbUserName"));
            mailConfig.setDbUserPassProperty(prop.getProperty("dbUserPassword"));

            found = true;
            LOG.info("file was found, properties were transformed to their fxBean");
        }

        return found;
    }

    /**
     * Creates a plain text properties file based on the parameters
     *
     * @param path Must exist, will not be created
     * @param propFileName Name of the properties file
     * @param mailConfig The bean to store into the properties
     * @throws IOException
     */
    public final boolean writeTextProperties(final String path, final String propFileName, final FXConfig mailConfig) throws IOException {
        Properties prop = new Properties();
        //validate if valid
        if (!isEmptyProp(mailConfig)) {
            prop.setProperty("userName", mailConfig.getUserNameProperty().get()); //person
            prop.setProperty("userPassword", mailConfig.getUserPassProperty().get()); //person
            prop.setProperty("userEmailAddress", mailConfig.getUserEmailAddressProperty().get()); //person

            prop.setProperty("imapServer", mailConfig.getImapServerProperty().get()); //receiver
            prop.setProperty("smtpServer", mailConfig.getSmtpServerProperty().get()); //sender
            prop.setProperty("imapPort", mailConfig.getImapPortProperty().get());
            prop.setProperty("smtpPort", mailConfig.getSmtpPortProperty().get());

            prop.setProperty("dbName", mailConfig.getDbNameProperty().get());
            prop.setProperty("dbPort", mailConfig.getDbPortProperty().get());
            prop.setProperty("url", mailConfig.getUrlProperty().get());

            prop.setProperty("dbUserName", mailConfig.getDbUserNameProperty().get());
            prop.setProperty("dbUserPassword", mailConfig.getDbUserPassProperty().get());

            Path txtFile = get(path, propFileName + ".properties");

            // Creates the file or if file exists it is truncated to length of zero
            // before writing
            try (OutputStream propFileStream = newOutputStream(txtFile)) {
                prop.store(propFileStream, "SMTP Properties");
            }
            LOG.info("Properties file is now created!");
            return true; // valid props
        } else {
            LOG.info("Invalid Properties were entered, please enter them again!");
            return false;

        }
    }// end of write

    /**
     * Helper method to validate properties
     *
     * @param mailConfig
     * @return true if at least 1 of the properties is empty, meaning not a
     * valid property
     */
    private boolean isEmptyProp(FXConfig mailConfig) {
        return (mailConfig.getUserNameValue().isEmpty()
                || mailConfig.getUserNameValue().isEmpty()
                || mailConfig.getUserPassValue().isEmpty()
                || mailConfig.getUserEmailAddressValue().isEmpty()
                || mailConfig.getImapServerValue().isEmpty()
                || mailConfig.getSmtpServerValue().isEmpty()
                || mailConfig.getImapPortValue().isEmpty()
                || mailConfig.getSmtpPortValue().isEmpty()
                || mailConfig.getDbNameValue().isEmpty()
                || mailConfig.getDbPortValue().isEmpty()
                || mailConfig.getUrlValue().isEmpty()
                || mailConfig.getDbUserNameValue().isEmpty()
                || mailConfig.getDbUserPassValue().isEmpty());
    }
}
