/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mez.myEmailProject.persistence;

import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.data.EmailBean;
import com.mez.myEmailProject.fxData.FXEmailBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1631492 Lara
 */
public class UpdateDAO {

    public final static org.slf4j.Logger LOG = LoggerFactory.getLogger(UpdateDAO.class);

    private final String databaseUrl;
    private final String password;
    private final String userName;
    private Util util = new Util();

    /**
     * constructor that will read from Property file
     *
     * @param dataBaseUrl
     * @param userName
     * @param pass
     * @param p
     */
    public UpdateDAO(String dataBaseUrl, String userName, String pass) {
        this.databaseUrl = dataBaseUrl;
        this.userName = userName;
        this.password = pass;
    }

    /**
     * update email in the draft folder
     *
     * @param updatedEmail
     * @param id
     * @throws java.sql.SQLException
     */
    public void updateDraft(FXEmailBean updatedEmail, int id) throws SQLException, IOException {
        DeleteDAO delete = new DeleteDAO(this.databaseUrl, this.userName, this.password);
        //delete the current email from the draft email_folder
        //create another email and return it to store in draft again
        delete.deleteAttachment(id);
        delete.deleteAllReceivers(id);
        delete.deleteEmail(id);

        //create another email
        StoreEmail store = new StoreEmail(this.databaseUrl, this.userName, this.password);
        store.storeEmail(new EmailBean(updatedEmail), "DRAFT");
    }

    /**
     * Move to another folder
     *
     * @param anotherFolder
     * @param id
     * @throws java.sql.SQLException
     */
    public void moveToAnotherFolder(String anotherFolder, int id) throws SQLException {

        String query = "UPDATE Folder_Email SET folderName = ? where email_id = ?";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            //add all email fields
            prepState.setString(1, anotherFolder);
            prepState.setInt(2, id);
            prepState.executeUpdate();
        }
    }
}
