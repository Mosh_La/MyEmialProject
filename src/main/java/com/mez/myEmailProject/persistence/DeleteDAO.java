/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mez.myEmailProject.persistence;

import com.mez.myEmailProject.Util.Util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1631492
 */
public class DeleteDAO {

    public final static org.slf4j.Logger LOG = LoggerFactory.getLogger(DeleteDAO.class);

    private final String databaseUrl;
    private final String password;
    private final String userName;
    private Util util = new Util();

    /**
     * constructor that will read from Property file
     *
     * @param dataBaseUrl
     * @param userName
     * @param pass
     * @param p
     */
    public DeleteDAO(String dataBaseUrl, String userName, String pass) {
        this.databaseUrl = dataBaseUrl;
        this.userName = userName;
        this.password = pass;
    }

    /**
     * Deletes selected folder
     *
     * @param folder
     */
    public void deleteFolder(String folder) {
        try (Connection con = DriverManager.getConnection(databaseUrl,
                userName,
                password)) {
            PreparedStatement prepState = con.prepareStatement("DELETE FROM FOLDER WHERE folderName = ?");
            prepState.setString(1, folder);
            prepState.executeUpdate();
        } catch (SQLException ex) {
            ex.getMessage();
        }
    }

    /**
     * Deletes a record from the attachment table
     *
     * @param id
     * @throws java.sql.SQLException
     */
    public void deleteAttachment(int id) throws SQLException {
        String query = "DELETE FROM Attachment WHERE EMAIL_ID = ?";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            prepState.setInt(1, id);
            prepState.executeUpdate();

        }
    }

    /**
     * Deletes a record from the receiver table
     *
     * @param id
     * @param email
     * @throws SQLException
     */
    public void deleteReceiver(int id, String email) throws SQLException {
        String query = "DELETE FROM receiversList WHERE EMAIL_ID = ? AND receiverEmailAddress = ?";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            prepState.setInt(1, id);
            prepState.setString(2, email);
            prepState.executeUpdate();

        }
    }

    /**
     * Deletes a record from the email table
     *
     * @param id
     * @throws SQLException
     */
    public void deleteEmail(int id) throws SQLException {
        String query = "DELETE FROM email WHERE EMAIL_ID = ?";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            prepState.setInt(1, id);
            prepState.executeUpdate();

        }
    }

    /**
     * Deletes everything from receivers
     *
     * @param id
     * @throws SQLException
     */
    public void deleteAllReceivers(int id) throws SQLException {
        String query = "DELETE FROM receiversList WHERE EMAIL_ID = ?";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            prepState.setInt(1, id);
            prepState.executeUpdate();

        }
    }
}
