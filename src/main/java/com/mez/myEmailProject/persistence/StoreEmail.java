/*
 * This class represents a stored email.
 * If stored: has at least one receiver, valid...
 */
package com.mez.myEmailProject.persistence;

import com.mez.myEmailProject.Util.Util;
import com.mez.myEmailProject.data.AttachmentBean;
import com.mez.myEmailProject.data.EmailBean;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.rowset.serial.SerialBlob;
import org.slf4j.LoggerFactory;

/**
 * @author user
 * @verison 1.0
 */
public class StoreEmail {

    public final static org.slf4j.Logger LOG = LoggerFactory.getLogger(StoreEmail.class);

    private final String databaseUrl;
    private final String password;
    private final String userName;
    private Util util = new Util();

    /**
     * Phase3 --> constructor that will read from Property file
     *
     * @param dataBaseUrl
     * @param userName
     * @param pass
     * @param p
     */
    public StoreEmail(String dataBaseUrl, String userName, String pass) {
        this.databaseUrl = dataBaseUrl;
        this.userName = userName;
        this.password = pass;
    }

    /**
     * This method will call all the helper methods which will store the
     * EmailBean in the emailTable, attachment (if any) and recepiants (if any)
     *
     * @param bean
     * @param folder
     * @return
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public int storeEmail(EmailBean bean, String folder) throws SQLException, IOException {

        int id = insertIntoEmailTable(bean);

        insertIntoReceiversTable(bean);

        insertIntoAttachmentTable(bean);

        insertIntoFolderEmail(bean, folder);

        return id;
    }

    /**
     * Used to insert the email_id in the appropriate folder
     *
     * @param email
     * @param folder
     * @throws java.sql.SQLException
     */
    public void insertIntoFolderEmail(EmailBean email, String folder) {
        String query = "INSERT INTO FOLDER_EMAIL VALUES (?,?)";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            prepState.setInt(1, email.getId());
            prepState.setString(2, folder);
            prepState.executeUpdate();
            LOG.info("Inserted into folder");
        } catch (SQLException ex) {
            Logger.getLogger(StoreEmail.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * This method will insert an Email instance to the email table, will call
     * helper methods to insert values to the receivers table and if any
     * attachments, will insert to the attachment table.
     *
     * @param emailBean
     * @return
     * @throws SQLException
     */
    public int insertIntoEmailTable(EmailBean emailBean) throws SQLException {
        //you would only store an email if it has at least ONE recepient
        if (!emailBean.getCc().isEmpty() || !emailBean.getBcc().isEmpty() || !emailBean.getToAddress().isEmpty()) {
            String query = "INSERT INTO EMAIL VALUES(0,?,?,?,?)";
            int recordNum = -1;
            try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
                PreparedStatement prepState = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                //add all email fields
                prepState.setString(1, emailBean.getSenderAddress());
                prepState.setString(2, emailBean.getSubject());
                prepState.setString(3, emailBean.getHtmlMessage());
                prepState.setString(4, emailBean.getPlainTextMessage());
                prepState.executeUpdate();
                try (ResultSet rs = prepState.getGeneratedKeys();) {
                    if (rs.next()) {
                        recordNum = rs.getInt(1);
                    }
                }
                emailBean.setId(recordNum);
                LOG.info("One record was added to the email table with id --> " + recordNum);
                return recordNum;

            }
        } else {
            LOG.info("No recepiants found..");
            throw new IllegalArgumentException("Email must have at least one recepient"
                    + " in order to be sent and stored in the database!!!!");
        }
    }

    /**
     * Will insert an attachment to the attachment table Type 1 = regular, Type
     * 2 = embedded
     *
     * @param emailBean
     * @throws java.sql.SQLException
     * @throws java.io.IOException
     */
    public void insertIntoAttachmentTable(EmailBean emailBean) throws SQLException, IOException {
        LOG.debug("Attchment creation entered");
        if (!emailBean.getAttachment().isEmpty()) {
            //if there is at least one regular attach
            for (AttachmentBean attachment : emailBean.getAttachment()) {
                createAttachmentRow(emailBean, attachment, 1);
            }
        }
        if (!emailBean.getEmbeddedAttachment().isEmpty()) {
            //if there is at least one regulat attach
            for (int i = 0; i < emailBean.getEmbeddedAttachment().size(); i++) {
                createAttachmentRow(emailBean, emailBean.getEmbeddedAttachment().get(i), 2);
                LOG.info("One record was added to the attachment table -  type: embedded Attach");

            }
        }

    }

    /**
     * Will insert receivers to the receiverList table Type 1 -> to, Type 2 ->
     * cc, Type 3 -> bcc
     *
     * @param emailBean
     * @throws SQLException
     */
    public void insertIntoReceiversTable(EmailBean emailBean) throws SQLException {
        if (!emailBean.getToAddress().isEmpty()) {
            //if there is at least one regulat attach
            for (int i = 0; i < emailBean.getToAddress().size(); i++) {
                createReceiversRow(emailBean, emailBean.getToAddress().get(i), 1);
                LOG.info("Email address: " + emailBean.getToAddress().get(i)
                        + " was added to the receiver table - type: to");

            }
        }

        if (!emailBean.getCc().isEmpty()) {
            //if there is at least one regulat attach
            for (int i = 0; i < emailBean.getCc().size(); i++) {
                createReceiversRow(emailBean, emailBean.getCc().get(i), 2);
                LOG.info("Email address: " + emailBean.getCc().get(i)
                        + " was added to the receiver table - type: cc");

            }
        }

        if (!emailBean.getBcc().isEmpty()) {
            //if there is at least one regulat attach
            for (int i = 0; i < emailBean.getBcc().size(); i++) {
                createReceiversRow(emailBean, emailBean.getBcc().get(i), 3);
                LOG.info("Email address: " + emailBean.getBcc().get(i)
                        + " was added to the receiver table B type: bcc");

            }
        }
    }

    /**
     * Helper method to generate a row in the attachment table
     *
     * @param emailBean
     * @param attach
     * @param type
     * @throws SQLException
     * @throws IOException
     */
    private void createAttachmentRow(EmailBean emailBean, AttachmentBean attach, int type) throws SQLException, IOException {
        //for every attachment belonging to this email, we will create a record in the database
        String query = "INSERT INTO attachment VALUES (0,?,?,?,?)";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            //add all email fields
            prepState.setBlob(1, new SerialBlob(attach.getAttachment()));
            prepState.setString(2, attach.getAttachmentName()); //store a byte array in db or somewhere else?
            prepState.setInt(3, type);
            prepState.setInt(4, emailBean.getId());
            prepState.executeUpdate();//Why does it silently fail here?
            //Test this without catching exceptions
        }
    }

    /**
     * Helper method to generate a row in the receivers table
     *
     * @param emailBean
     * @param receiverEmail
     * @param type
     * @throws SQLException
     */
    private void createReceiversRow(EmailBean emailBean, String receiverEmail, int type) throws SQLException {
        String query = "INSERT INTO ReceiversList VALUES (?,?,?)";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(query);
            //add all email fields
            prepState.setInt(1, emailBean.getId());
            prepState.setInt(2, type);
            prepState.setString(3, receiverEmail);
            prepState.executeUpdate();
        }
    }

    /**
     * Add a folder
     * @param folderName
     * @throws java.sql.SQLException
     */
    public void addFolder(String folderName) throws SQLException {
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement("INSERT INTO FOLDER VALUES (?)");
            prepState.setString(1, folderName);
            prepState.executeUpdate();
    }
}
}
