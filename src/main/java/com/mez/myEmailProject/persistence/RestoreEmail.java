/*
 *This is a class that will retreive an email from the database.
 */
package com.mez.myEmailProject.persistence;

import com.mez.myEmailProject.data.EmailBean;
import java.sql.SQLException;
import java.util.List;
import com.mez.myEmailProject.data.AttachmentBean;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javafx.scene.control.ListView;
import org.slf4j.LoggerFactory;

/**
 * @author userName
 * @version 1.0
 */
public class RestoreEmail implements Serializable {

    public final static org.slf4j.Logger LOG = LoggerFactory.getLogger(RestoreEmail.class);
    private final String databaseUrl;
    private final String password;
    private final String userName;

    /**
     * Phase3- this constructor will be used to read the configuration of each
     * user
     *
     * @param dataBaseUrl
     * @param userName
     * @param pass
     * @param p
     */
    //for the testing phase2
    public RestoreEmail(String dataBaseUrl, String userName, String pass) {
        this.databaseUrl = dataBaseUrl;
        this.userName = userName;
        this.password = pass;
    }

    /**
     * Finds the id's of all emails that are stored in a given folder
     *
     * @param name
     * @return
     * @throws java.sql.SQLException
     */
    public List<Integer> findIdsForGivenFolder(String name) throws SQLException {
        List<Integer> list = new ArrayList();
        String selectQuery = "SELECT * FROM FOLDER_EMAIL WHERE folderName = ?";
        //using try ressources to ensure that the obkect will always be closed even if there is an exception
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(selectQuery);
            prepState.setString(1, name);
            try (ResultSet resultSet = prepState.executeQuery()) {
                while (resultSet.next()) {
                    list.add(resultSet.getInt("email_id"));
                }
            }
        } catch (SQLException sql) {
            sql.getMessage();
        }
        return list;
    }

    /**
     * Retreive an email by the given id
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public EmailBean getEmailByEmailId(int id) throws SQLException {
        //create the email bean that will be returned
        EmailBean resultEmail = new EmailBean();

        String selectQuery = "SELECT * FROM EMAIL WHERE email_id = ?";

        //using try ressources to ensure that the obkect will always be closed even if there is an exception
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(selectQuery);
            prepState.setString(1, String.valueOf(id));
            try (ResultSet resultSet = prepState.executeQuery()) {
                while (resultSet.next()) {
                    //adding basic components to email
                    resultEmail = createEmailBean(resultEmail, resultSet);
                    //adding receiver's seperatly since they are stored in a different table
                    //at least one method wont return an emptyList, since there must be at least one receiver
                    resultEmail.setToAddress(getReceiversInfo(id, 1)); // 1-> to
                    resultEmail.setCc(getReceiversInfo(id, 2)); //2 -> cc
                    resultEmail.setBcc(getReceiversInfo(id, 3)); // 3-> bcc
                    ///if any attachments, adding seperatly 
                    resultEmail.setAttachment(getAttachments(id, 1)); // 1 --> regular
                    resultEmail.setEmbeddedAttachment(getAttachments(id, 2)); // 2--> embedded                 
                }
            }
        }
        LOG.info("The requested id is: " + id + " the returned email has id " + resultEmail.getId());
        return resultEmail;
    }

    /**
     * A helper method that will retreive an attachment from the db -->
     * depending on the type and the id of the email
     *
     * @param id
     * @param type
     * @return
     * @throws SQLException
     */
    private List<AttachmentBean> getAttachments(int id, int type) throws SQLException {
        //will return Receiver's passwords or Receiver's emails from db
        List<AttachmentBean> result = new ArrayList<>(0); //will return 0 if not found
        String selectQuery = "SELECT * FROM attachment WHERE email_id = ? AND attachType = ?";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(selectQuery);
            prepState.setInt(1, id); //select
            prepState.setInt(2, type); //where
            try (ResultSet resultSet = prepState.executeQuery()) {
                while (resultSet.next()) {
                    result.add(createAttachment(resultSet, type));
                }
            }
        }
        return result;
    }

    /**
     * This method will create an email bean to store all we got from the
     * database
     *
     * @param e
     * @param rs
     * @return
     * @throws SQLException
     */
    private EmailBean createEmailBean(EmailBean e, ResultSet rs) throws SQLException {
        //get basic components from db
        int id = rs.getInt(1);
        String senderAddress = rs.getString("senderEmailAddress");
        String subject = (rs.wasNull()) ? "" : rs.getString("subject");
        String plainTextMessage = (rs.wasNull()) ? "" : rs.getString("plainText");
        String htmlMessage = (rs.wasNull()) ? "" : rs.getString("htmlText");
        //set email object with the gotten components 
        e.setId(id);
        e.setUserName(this.userName);
        e.setSenderAddress(senderAddress);
        e.setSenderPass(this.password);
        e.setSubject(subject);
        e.setPlainTextMessage(plainTextMessage);
        e.setHtmlMessage(htmlMessage);
        LOG.info("email bean was created");
        return e;
    }

    /**
     * Retreive the receivers by querying the id of the email and the type of
     * the receiver
     *
     * @param id
     * @param type
     * @return
     * @throws SQLException
     */
    private List<String> getReceiversInfo(int id, int type) throws SQLException {
        //will return Receiver's passwords or Receiver's emails from db
        List<String> result = new ArrayList<>(0); //will return 0 if not found
        String selectQuery = "SELECT receiverEmailAddress FROM receiversList WHERE email_id = ? AND receiverType = ?;";
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement(selectQuery);
            prepState.setInt(1, id); //from
            prepState.setInt(2, type); //where
            try (ResultSet resultSet = prepState.executeQuery()) {
                while (resultSet.next()) {
                    result.add(createData(resultSet));
                }
            }
        }
        LOG.info("return list of receivers of size " + result.size() + " of type " + type);
        return result;
    }

    /**
     * Helper method to return the receiver's email
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private String createData(ResultSet rs) throws SQLException {
        return rs.getString("receiverEmailAddress");
    }

    /**
     * A method to create an attachmentBean from the db's blob datatype
     *
     * @param rs
     * @param type
     * @return
     * @throws SQLException
     */
    private AttachmentBean createAttachment(ResultSet rs, int type) throws SQLException {
        Blob content = rs.getBlob("content"); //have to convert to bytes later
        String attachName = rs.getString("name");
        //convert blob to byte[] and free the blob ressource
        byte[] contentByte = content.getBytes(1, (int) content.length());
        content.free();
        //convert attachType to boolean from int
        boolean embedded = (type == 1) ? false : true;
        AttachmentBean attach = new AttachmentBean(attachName, contentByte, "", embedded);
        LOG.info("Attachment if type " + embedded + " is now created");
        return attach;
    }

    /**
     * Selects all folders, and add them to the listView
     *
     * @param listOfFolders
     * @return list(String) of all the folders
     */
    public List<String> getAllFolders(ListView<String> listOfFolders) {
        List<String> folders = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(databaseUrl, userName, password)) {
            PreparedStatement prepState = con.prepareStatement("SELECT * FROM FOLDER");
            try (ResultSet resultSet = prepState.executeQuery()) {
                while (resultSet.next()) {
                    listOfFolders.getItems().add(resultSet.getString("folderName"));
                    folders.add(resultSet.getString("folderName"));
                }
            }

        } catch (SQLException ex) {
            ex.getMessage();
        }
        return folders;

    }
}
